/** *******
 * dev.js
 *********/

//for   prod host we must add  wwww before the link
//const MAILLINK = config.env === './production' ? 'https://www.' + config.hostname : mailLink;
const envConfig = {
    hostname: process.env.IP || 'localhost',
    port: process.env.PORT || 4000,
    databaseUri: process.env.DATABASE_URI || 'mongodb://127.0.0.1:27017/jobaas',
    databaseName: 'jobaas',
    populateDatabase: process.env.POPULATE_DATABASE || true,
    IP_ADMIN: ['93.24.110.45'],
    API_KEY_GOOGLE_MAP: 'AIzaSyDnP_1G42kry1kwA-Ydl9tv3r8pV6KadoQ',
    jwt_secret: process.env.JWT_SECRET || '09f26e402586e2faa8da4c98a35f1b20d6b033c6097befa8be3486a829587fe2f90a832bd3ff9d42710a4da095a2ce285b009f0c3730cd9b8e1af3eb84df6611',
    sr: process.env.SR || 12,
    jwt_expiration_in_seconds: process.env.EXPIRE_TIME || 86400,
    rate_limit_in_milliseconds: process.env.RATELIMITER_TIME || 300000,
    rate_block_limit_seconds: process.env.RATEBLOCKLIMITER_TIME || 180,
    no_mail: process.env.NO_MAIL || 0,
    jobPriceMin: 50,
    no_sms: process.env.NO_SMS || 1,
    user_api_sms: process.env.USER_API_SMS || 'cto@jobaas.cm',
    password_api_sms: process.env.PASSWORD_API_SMS || 'zerty123',
    phonenumbers: 690637982,
    redis_url: process.env.REDIS_URL || 'redis://h:p5b2b873b7de3f2b7d2ff29e1a4a2e016acc1ac37dc1289c330a81bd899d4f546@ec2-54-209-85-193.compute-1.amazonaws.com:21549',
    permissionLevels: {
        'EMPLOYER_USER': 5,
        'EMPLOYEE_USER': 7,
        'ENTREPRISE_USER': 10,
        'ADMIN': 2048,
        'CONTROLLER_USER': 777,
        'SUP_ADMIN': 1468
    },
    mailgun_apikey: process.env.MAILGUN_API_KEY || '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',
    mailgun_domain: process.env.MAILGUN_DOMAIN || 'jobaas.cm',
    mailgun_host: process.env.MAILGUN_HOST || 'api.eu.mailgun.net',
    mailLink:process.env.MAILLINK ||'http://localhost:4000',
    contactus_email: process.env.CONTACTUS_EMAIL ||"info@jobaas.cm",
    cto_mail: process.env.CTO_MAIL || 'jordanetsafack@gmail.com',
    customers_service_mails:process.env.CUSTOTMERS_SERVICE_MAILS  || 'jordanetsafack@gmail.com'.split(","),
    it_mails:process.env.IT_MAILS  || 'jordanetsafack@gmail.com'.split(","),
    marketing_mails: process.env.MARKETING_MAILS || "jordanetsafack@gmail.com".split(","),
    allowedOrigins:process.env.ALLOWEDORIGINS || ['https://www.jobaas.cm', 'https://www.jobaas.fr',
    'https://jobaas-backoffice.herokuapp.com'],
    logsene_token: process.env.LOGSENE_TOKEN || '14518b95-443d-41ee-b2e4-06c228b72d30',
    exception_transaction_process: process.env.EXCEPTION_TRANSACTION || Number('0'),
    bucketName: "tutorial1aws"
};

module.exports = envConfig;
