/** *******
 * production.js
 *********/

const envConfig = {
    hostname: 'jobaas.cm',
    port: process.env.PORT,
    databaseUri: process.env.DATABASE_URI,
    databaseName: process.env.DATABASE_NAME,
    populateDatabase: false,
    jwt_secret: process.env.JWT_SECRET,
    IP_ADMIN: process.env.IP_ADMIN,
    jobPriceMin: 1000,
    API_KEY_GOOGLE_MAP: process.env.API_KEY_GOOGLE_MAP,
    jwt_expiration_in_seconds: process.env.EXPIRE_TIME,
    rate_limit_in_milliseconds: process.env.RATELIMITER_TIME,
    rate_block_limit_seconds: process.env.RATEBLOCKLIMITER_TIME,
    sr: process.env.SR,
    no_mail: process.env.NO_MAIL || 1,
    no_sms: process.env.NO_SMS || 1,
    contactus_email: process.env.CONTACTUS_EMAIL ||"info@jobaas.cm",
    it_mails:process.env.IT_MAILS.split(",") || 'leonelelanga@yahoo.fr, jordanetsafack@gmail.com'.split(","),
    cto_mail: process.env.CTO_MAIL,
    marketing_mails: process.env.MARKETING_MAILS.split(",") || "lgatsing@gmail.com".split(","),
    allowedOrigins:process.env.ALLOWEDORIGINS.split(",") || ['https://www.jobaas.cm', 'https://www.jobaas.fr',
    'https://jobaas-backoffice.herokuapp.com'],
    redis_url: process.env.REDIS_URL,
    permissionLevels: {
        'EMPLOYER_USER': 5,
        'EMPLOYEE_USER': 7,
        'ENTREPRISE_USER': 10,
        'ADMIN': 2048,
        'CONTROLLER_USER': 777,
        'SUP_ADMIN': 1468
    },
    mailgun_apikey: process.env.MAILGUN_API_KEY,
    mailgun_domain: process.env.MAILGUN_DOMAIN,
    mailgun_host: process.env.MAILGUN_HOST,
    mailLink:process.env.MAILLINK ||'http://' + config.hostname + ':' + config.port,
    user_api_sms: process.env.USER_API_SMS,
    password_api_sms: process.env.PASSWORD_API_SMS,
    phonenumbers: 690637982,
    logsene_token: process.env.LOGSENE_TOKEN,
    exception_transaction_process: process.env.EXCEPTION_TRANSACTION || Number('0'),
    bucketName: process.env.bucketNameProd
};

module.exports = envConfig;
