/** *******
 * release.js
 *********/

const envConfig = {
    hostname: process.env.IP || 'jobaas-backend-dev.herokuapp.com',
    port: process.env.PORT,
    databaseUri: process.env.DATABASE_URI,
    databaseName: process.env.DATABASE_NAME,
    populateDatabase: process.env.POPULATE_DATABASE || false,
    jwt_secret: process.env.JWT_SECRET,
    IP_ADMIN: process.env.IP_ADMIN,
    jobPriceMin: 50,
    API_KEY_GOOGLE_MAP: process.env.API_KEY_GOOGLE_MAP,
    no_mail: process.env.NO_MAIL || 1,
    no_sms: process.env.NO_SMS || 1,
    jwt_expiration_in_seconds: process.env.EXPIRE_TIME,
    rate_limit_in_milliseconds: process.env.RATELIMITER_TIME,
    rate_block_limit_seconds: process.env.RATEBLOCKLIMITER_TIME,
    sr: process.env.SR,
    contactus_email: process.env.CONTACTUS_EMAIL ||"info@jobaas.cm",
    cto_mail: process.env.CTO_MAIL,
    it_mails:process.env.IT_MAILS  || 'leonelelanga@yahoo.fr, jordanetsafack@gmail.com',
    marketing_mails: process.env.MARKETING_MAILS.split(",") || "jordanetsafack@gmail.com",
    allowedOrigins:process.env.ALLOWEDORIGINS.split(",") || ['https://www.jobaas.cm', 'https://www.jobaas.fr',
    'https://jobaas-backoffice.herokuapp.com'],
    redis_url: process.env.REDIS_URL,
    permissionLevels: {
        'EMPLOYER_USER': 5,
        'EMPLOYEE_USER': 7,
        'ENTREPRISE_USER': 10,
        'ADMIN': 2048,
        'CONTROLLER_USER': 777,
        'SUP_ADMIN': 1468
    },
    mailgun_apikey: process.env.MAILGUN_API_KEY,
    mailgun_domain: process.env.MAILGUN_DOMAIN,
    mailgun_host: process.env.MAILGUN_HOST,
    mailLink:process.env.MAILLINK ||'http://' + config.hostname + ':' + config.port,
    user_api_sms: process.env.USER_API_SMS,
    password_api_sms: process.env.PASSWORD_API_SMS,
    bucketName: process.env.bucketName,
    AWS_SECRET_ACCESS_KEY: process.env.AWS_SECRET_ACCESS_KEY,
    AWS_ACCESS_KEY_ID: process.env.AWS_ACCESS_KEY_ID,
    AWS_REGION: process.env.AWS_REGION,
    phonenumbers: 690637982,
    logsene_token: process.env.LOGSENE_TOKEN,
    exception_transaction_process: process.env.EXCEPTION_TRANSACTION || Number('0'),
};

module.exports = envConfig;
