let chai = require('chai');
let server = require('../../server');
const supertest = require('supertest');
const should = chai.should();
const api = supertest.agent(server);
const subscription = 'subscription';
const baseUrl = '/api/v1/';
const url = baseUrl + subscription;
global.subscriptionId = "";
global.subscriptionId2 = "";


//test insertion for an subscription 
describe('POST '+url, () => {
    it('should respond with a success message along with a single subscription that was added', (done) => {
        api.post(url + '?lang=en')
            .send(
                {
                    "field1": "value1",
                    "field2": "value2",
                    "field3": "value3",
                    "field4": "value4",
                    "field5": "value5"
                }).set('authorization', 'Bearer ' + global.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                // (indicating that something was "created")
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                //check the message content
                res.body.message.should.eql('new subscription was created');

                global.subscriptionId = res.body.data.id;
                done();
            });
    });
});




// get the subscription list shouldn't be empty cause we just add one subscription previously
describe('GET '+url, () => {
    it('should respond with a success message along with a get on all the  subscriptions received that were added', (done) => {
        api.get(url + '?lang=en')
            .set('authorization', 'Bearer ' + global.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                //the numbers of subscriptions objects should be not null
                done();
            });
    });
});


//get a single subscription

describe('GET '+url+'/:id', () => {
    it('should respond with a success message along with a single get on a subscription that was added', (done) => {
        api.get(url + '/' + global.subscriptionId + '?lang=en')
            .set('authorization', 'Bearer ' + global.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                // (indicating that something was "created")
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');

                done();
            });
    });
});

//get a single subscription with false id

describe('GET '+url+'/:id 2', () => {
    const fakeId = '5ds';
    it('should respond with a error message along with a single subscription that wasn t added', (done) => {
        api.get(url + '/' + fakeId + '?lang=en')
            .set('authorization', 'Bearer ' + global.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 404 status code
                res.status.should.equal(500);
                // the response should be JSON
                res.type.should.equal('application/json');
                //check the respond code
                done();
            });
    });
});


//get all the evulations from  a specific jober
describe('GET /api/v1/particular/:userId/subscriptions?lang=en', () => {
    it('should respond with a success message along with all subscriptions from a specific jober ', (done) => {
        api.get('/api/v1/particular/' + global.idJober + '/subscriptions?lang=en')
            .set('authorization', 'Bearer ' + global.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                // (indicating that something was "created")
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                //check the message content
                res.body.message.should.eql('All the subscriptions found for the particular id');

                done();
            });
    });
});


//update a subscription  , here we first save the id of the subscription from the previous get request
describe('UPDATE '+url, () => {
    const grade = 1;
    it('should respond with a success message along with a single subscription that was updated', (done) => {
        api.put(url + '/' + global.subscriptionId + '?lang=en')
            .set('authorization', 'Bearer ' + global.token)
            .send(                {
                    "field1": "value1",
                    "field2": "value2",
                    "field3": "value3",
                    "field4": "value4",
                    "field5": "value5"
                })
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                // (indicating that something was "updated")
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');

                res.body.data.grade.should.equal(grade);
                done();
            });
    });
});

describe('DELETE /api/v1/subscription', () => {
    it('should respond with a success message along with a single subscription that was deleted', (done) => {
        api.delete(url + '/' + global.subscriptionId + '?lang=en')
            .set('authorization', 'Bearer ' + global.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                // (indicating that something was "deleted")
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                //check on message
                res.body.message.should.equal('the subscription was deleted by id : ' + global.subscriptionId);
                done();
            });
    });
});



