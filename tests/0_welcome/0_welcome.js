 const config = require('../../config/environnement/config');
const mongoose = require('mongoose');
const should = require('chai').should();

describe('TESTS STARTED', function () {
    before(async function (done) {
        function clearCollections() {
            for (let collection in mongoose.connection.collections) {
                mongoose.connection.collections[collection].deleteMany(function () {
                });
            }
            return done();
        }
        if (mongoose.connection.readyState === 0) {

            mongoose.connect(config.databaseUri,
                {
                    useNewUrlParser: true,
                    useUnifiedTopology: true
                },
                function (err) {
                    if (err) throw err;
                });
        }
        clearCollections();

        console.log('prepare of TU');
    });

    describe(' Var env should be defined', () => {
        let test = -1;
        it('database env test shoud return 2', () => {
            if (config.databaseUri && config.databaseName) {
                test = 2;
            }
            test.should.be.equal(2);
        });
        it('jwt env test shoud return 3', () => {
            if (config.jwt_secret && config.jwt_expiration_in_seconds && config.sr) {
                test = 3;
            }
            test.should.be.equal(3);
        });
        it('mailgun test shoud return 4', () => {
            if (config.mailgun_apikey && config.mailgun_domain && config.mailgun_host) {
                test = 4;
            }
            test.should.be.equal(4);
        });
        it('sms key test shoud return 5', () => {
            if (config.user_api_sms && config.password_api_sms && config.phonenumbers) {
                test = 5;
            }
            test.should.be.equal(5);
        });
        it('rate limiter test shoud return 6', () => {
            if (config.rate_block_limit_seconds && config.rate_limit_in_milliseconds && config.redis_url) {
                test = 6;
            }
            test.should.be.equal(6);
        });
        it('IP_ADMIN test shoud return 7', () => {
            if (config.IP_ADMIN) {
                test = 7;
            }
            test.should.be.equal(7);
        });
        it(' API_KEY_GOOGLE_MAP test shoud return 8', () => {
            if (config.API_KEY_GOOGLE_MAP) {
                test = 8;
            }
            test.should.be.equal(8);
        });
        it(' NO MAIL VAR ENV test shoud return 9', () => {
            if (config.no_mail || Number(config.no_mail) === 0) {
                test = 9;
            }
            test.should.be.equal(9);
        });
        it(' CTO EMAIL test shoud return 10', () => {
            if (config.cto_mail) {
                test = 10;
            }
            test.should.be.equal(10);
        });
        it('LOG SENE test shoud return 11', () => {
            if (config.logsene_token) {
                test = 11;
            }
            test.should.be.equal(11);
        });
    });
});
