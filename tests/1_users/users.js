
let chai = require('chai');
let server = require('../../src/index');
const supertest = require('supertest');
const should = chai.should();
const api = supertest.agent(server);
const serviceName = 'users';
const baseUrl = '/api/v1/';
const url = baseUrl + serviceName;
//token information
const urlToken = baseUrl + "auth/login";


//users infos
//Job Provicer
global.user1Email = "leonel@yahoo.fr"; //jobaas.cm";//"@yahoo.fr";
global.nameUser1 = "ELANGA NKOO";
global.surnameUser1 = "Steve Patrick Lionel";

//User2
global.user2Email = "elangal@3il.fr";//jobaas.cm";//"gmail.com";
global.nameUser2 = "Ulrich";
global.surnameUser2 = "BAZOU";
global.password = "string9S#";

//User3
global.User3Email = "stevepatricklionel.elanga@gmail.com";//"gmail.com";
global.nameUser3 = "NKO'O";
global.surnameUser3 = "Steve";


//User4
global.User4Email = "zendaya@gmail.com";//"gmail.com";
global.nameUser4 = "Zendaya";
global.surnameUser4 = "NKAM";

//identifiers to insert  while testing  Job, Application and so on.
global.idUser1 = "";
global.idUser2 = "";
global.idUser3 = "";
global.idUser4 = "";
global.idAdmin = "";
global.idController = "";

//Token
global.token = "";
global.tokenUser2 = "";
global.tokenUser3 = "";
global.tokenUser4 = "";
global.tokenAdmin = "";
global.tokenController = "";
global.jobuserId2 = "";

// affiliation 
global.codeAffiliation = "";

//transactionsId
global.transactionIdFirstPayment = "";
global.transactionIdSecondPayment = "";
global.transactionIdFinalPayment = "";

//test user insertion
describe('create_first_User1', () => {
    it('should respond with a success message along with a single user that was added', (done) => {
        api.post(url + '?lang=en')
            .send(
                {
                    "name": global.nameUser1,
                    "surname": global.surnameUser1,
                    "description": "Je suis " + global.nameUser1 + " " + global.surnameUser1 + " contacte  moi à  " + global.user1Email,
                    "MoneyAccount": [33786177031],
                    "phoneNumber": {"value": 699820046},
                    "email": {"value": global.user1Email},
                    "valid": false,
                    "gender": "Man",
                    "schoolLevel": {"level": "bac", "diplomaYear": 2014},
                    "password": global.password,
                    "software": ["eclipse"],
                    "language": [{"value": "English", "level": "beginner"}, {"value": "French", "level": "maternal"}],
                    "country": "Cameroun",
                    "town": "Bafoussam",
                    "street": "Explosif",
                    "referenceStreet": "Collège Saint Thomas",
                    "state": ["employee"],
                    "profession": "student",
                    "birthday": "1999-03-15"
                   
                })
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                //check the message content
                global.idUser1 = res.body.data.id;
                done();
            });
    });
});

//test affiliation code existence
describe('check_affiliationcode_first_User1', () => {
    it('should respond with a success message along with a get on single user', (done) => {
        api.get(url+"/"+global.idUser1+'?lang=en')
           .set('authorization', 'Bearer ' + global.tokenAdmin)
           .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                //check the message content
                res.body.data.user.affiliation.code.should.be.not.empty ;             
                global.codeAffiliation = res.body.data.user.affiliation.code;
                done();
            });
    });
});


//test  add a second user insertion
describe('create_first_User2', () => {
    it('should respond with a success message along with a single partiular that was added', (done) => {
        api.post(url + '?lang=en')
            .send(
                {
                    "name": global.nameUser2,
                    "surname": global.surnameUser2,
                    "description": "Je suis " + global.nameUser2 + " " + global.surnameUser2 + "contact me at " + global.User2Email,
                    "MoneyAccount": [33786177030],
                    "phoneNumber": {"value": 699820047},
                    "email": {"value": global.User2Email},
                    "driver_permit": {"vehicle": "car", "date": 1999, "category": "A"},
                    "valid": false,
                    "gender": "Man",
                    "schoolLevel": {"level": "bac+2", "diplomaYear": 2014},
                    "profilePic": null,
                    "password": global.password,
                    "country": "Cameroun",
                    "town": "Douala",
                    "street": "Boulangerie Alfred Saker",
                    "referenceStreet": "Collège Saint Thomas",
                    "state": ["employee"],
                    "profession": "student",
                    "skills": ["programmation"],
                    "software": ["eclipse"],
                    "birthday": "1999-03-15",
                    "affiliation":{from:global.codeAffiliation}
                })
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                global.idUser2 = res.body.data.id;
                done();
            });
    });
});


//test affiliation incrementation after previous registration  insertion
describe('check_affiliation.count_increase_due_previous_registration', () => {
    it('should respond with a success message along with a get on single user', (done) => {
        api.get(url+"/"+global.idUser1+'?lang=en')
           .set('authorization', 'Bearer ' + global.tokenAdmin)
           .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                //check the message content
                res.body.data.user.affiliation.count.should.equal(1);
                done();
            });
    });
});

//test  add a fourth user insertion
describe('create_second_User2', () => {
    it('should respond with a success message along with a single user that was added', (done) => {
        api.post(url + '?lang=en')
            .send(
                {
                    "name": global.nameUser3,
                    "surname": global.surnameUser3,
                    "description": "Je suis " + global.nameUser3 + " " + global.surnameUser3 + "contact me at " + global.User3Email,
                    "MoneyAccount": [33786177033],
                    "phoneNumber": {"value": 656312801},
                    "email": {"value": global.User3Email},
                    "valid": false,
                    "gender": "Man",
                    "tags": ['Others'],
                    "schoolLevel": {"level": "bac+3", "diplomaYear": 2013},
                    "password": global.password,
                    "country": "Cameroun",
                    "town": "Douala",
                    "street": "Boulangerie Alfred Saker",
                    "referenceStreet": "Collège Saint Thomas",
                    "state": ["employee"],
                    "profession": "teacher",
                    "birthday": "1999-03-15"
                }
            )
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                //check the message content
                global.idUser3 = res.body.data.id;
                done();
            });
    });
});

//test  add a fifth user insertion
describe('create_an_user_with_invalid_password', () => {
    it('should respond with a message that password is not valid along with the form filled by the user', (done) => {
        api.post(url + '?lang=en')
            .send(
                {
                    "name": "Osef",
                    "surname": "Like",
                    "description": "Je suis un faux utilisateur",
                    "MoneyAccount": [33999999999],
                    "phoneNumber": {"value": 237999999999},
                    "email": {"value": "osef_like_really@gmail.com"},
                    "valid": false,
                    "gender": "Man",
                    "tags": ['Others'],
                    "schoolLevel": {"level": "bac+3", "diplomaYear": 2013},
                    "password": "string",
                    "country": "Cameroun",
                    "town": "Douala",
                    "street": "Boulangerie Alfred Saker",
                    "referenceStreet": "Collège Saint Thomas",
                    "state": ["employee"],
                    "profession": "teacher",
                    "birthday": "1999-03-15"
                }
            )
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(400);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                //check the message content
                // res.body.message.should.eql('Invalid password. there should be at least 8 characters ' +
                //     'and at least one number, one capital and lower character, ' +
                //     'and one no alphanumerique character among [!%#_?@]');
                done();
            });
    });
});

// non valid mail  cannot connect
//get the token User2
describe('authenticate_user_with_boolean_email_at_false', () => {
    it('should respond with a warning  when non valid  User2  request for token with good credential', (done) => {
        api.post(urlToken + '/user?lang=en')
            .send({
                "email": global.User3Email,
                "password": global.password
            })
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(403);
                res.type.should.equal('application/json');
                done();
            });
    });
});


//test  add a fifth user insertion
describe('create_third_jobber', () => {
    it('should respond with a success message along with a single user that was added', (done) => {
        api.post(url + '?lang=en')
            .send(
                {
                    "name": global.nameUser4,
                    "surname": global.surnameUser4,
                    "description": "Je suis " + global.nameUser4 + " " + global.surnameUser4 + "contact me at " + global.User4Email,
                    "MoneyAccount": [33786177033],
                    "phoneNumber": {"value":697040469},
                    "email": {"value": global.User4Email},
                    "valid": false,
                    "gender": "Woman",
                    "tags": ['Others'],
                    "schoolLevel": {"level": "bac+3", "diplomaYear": 2013},
                    "password": global.password,
                    "country": "Cameroun",
                    "town": "Douala",
                    "street": "Boulangerie Alfred Saker",
                    "referenceStreet": "Collège François Xavier Vogt",
                    "state": ["employee"],
                    "profession": "teacher",
                    "birthday": "1998-10-15"
                }
            )
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                //check the message content
                global.idUser4 = res.body.data.id;
                done();
            });
    });
});

