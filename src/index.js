
const express = require('express')
const config = require('./../config/environnement/config');
const app = require("./common/core/runner/app")
const loggerGenerator = require("./common/core/logger/logger")
const constants = require("./common/core/constants/constants")
const port = 3000;
const  layerName= "main"
const  controllerName ="index"
const logger = loggerGenerator.getLoger(layerName,controllerName)




// create the basic server setup
app.create(config);
logger.info("Start appli "+constants.business.APP_NAME)
// start the server
let server = app.start(config.databaseUri);
module.exports = server;



