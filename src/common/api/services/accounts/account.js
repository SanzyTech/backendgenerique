// CONFIG 
const config = require('../../../../../config/environnement/config');
const no_mail = config.no_mail;
const no_sms = 0 //config.no_sms;

// PRE/PROCESSING
const formatFilterParams = require('../../../core/processing/processing').formatFilterParams;
const processing = require('../../../core/processing/processing');
const preprocessing = require('../../../core/processing/cleanDescription');

// MODEL
const UserModel = require('../../models/user');

//SERVICE 
const roleService = require('./role');
const affiliationService = require('./affiliation');
const mailService = require('../channels/email');
const mailFormatter =   require('../../../core/channelTemplates/mails/formatter/mailFactory') ;
const smsFormatter =   require('../../../core/channelTemplates/sms/formatter/smsFactory') ;
// LOG 
const loggerGenerator = require("../../../core/logger/logger") ;
const constants = require("../../../core/constants/constants");
const version = constants.technical.VERSION_1 ;
const  layerName = constants.technical.LAYER_SERVICE ;
const  servicerName = constants.technical.ACCOUNT ;
const logger = loggerGenerator.getLoger(layerName,servicerName) ;

class Account
{


  constructor(){
 
    }

  setGetUser(getUser){

    this.getUser =getUser ;
  

  } ;

  setUpdateUser(updateUser){
    this.updateUser = updateUser ;

  } ;

  setRequestIdToTheCurrentAccount(currentAccount, requestId){
        let dt = new Date();
        dt.setMinutes(dt.getMinutes() + 20);
        dt = dt.toISOString();
        currentAccount.phoneNumber.requestIdExpired = dt;
        currentAccount.phoneNumber.requestId = requestId;
        return currentAccount;
};

    async generateSmsCode(userId, getUser,updateUser) {
        // this function generate a code that will be send by sms  by another function to check user number 
        let currentAccount = await getUser('id', userId);
        const requestId = Math.floor(100000 + Math.random() * 900000);
        currentAccount = this.setRequestIdToTheCurrentAccount(currentAccount, requestId);
        await updateUser(userId,{"phoneNumber": currentAccount.phoneNumber});
        return requestId;
    };

    async sendActivationLink(user){

        // SEND MAIL AND SMS TO USER IN ORDER TO ACTIVATE  ACCOUNT

        // MAIL 
        await this.sendActivationMail(user) ;
        // SMS
        await this.sendActivationSms (user) ;

    } ;

    async sendActivationMail(user){

        // SEND MAIL AND SMS TO USER IN ORDER TO ACTIVATE  ACCOUNT

        // MAIL 
        const apiLink = `${config.mailLink}/${constants.technical.API}/${version}/${constants.technical.USER}/${user._id}/verify_account?lang=fr&token=${user.validationToken.token}`;
        
        logger.info('apiLink : ' + apiLink);

        const registrationMail = new  mailFormatter.RegistrationMail(user.name , user.email.value, apiLink);
        logger.info("no mail in user controller " + no_mail);
        if (Number(no_mail) === 0) {
            logger.info('after the no mail condition');
        await  registrationMail.send() ;
        }


    } ;

    async sendActivationSms (user){
      
    const smsCode = await  this.generateSmsCode(user._id, this.getUser,this.updateUser);
    const apiLink =`${config.mailLink} /${constants.technical.API}/${constants.technical.USER}/${version}/${user._id}/verify_phonenumber?lang=fr&pin=${smsCode}`;
    logger.info(" validation number "+apiLink);

    const registrationSms = new   smsFormatter.RegistrationSms(user.name , user.phoneNumber.value, apiLink);

    logger.info("no sms in user controller " + no_sms);

    if (Number(no_sms) === 0) {
            logger.info('after the no sms condition');
            await  registrationSms.send() ;
    }

    } ;





}

module.exports =  new Account();
