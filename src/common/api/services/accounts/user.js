
// CONFIG 
const config = require('../../../../../config/environnement/config');
const no_mail = config.no_mail;
const no_sms = 0 //config.no_sms;

// PRE/PROCESSING
const formatFilterParams = require('../../../core/processing/processing').formatFilterParams;
const processing = require('../../../core/processing/processing');
const preprocessing = require('../../../core/processing/cleanDescription');

// MODEL
const UserModel = require('../../models/user');

//SERVICE 
const roleService = require('./role');
const affiliationService = require('./affiliation');
const accountService = require('./account');
const mailService = require('../channels/email');
const mailFormatter =   require('../../../core/channelTemplates/mails/formatter/mailFactory') ;
const smsFormatter =   require('../../../core/channelTemplates/sms/formatter/smsFactory') ;
// LOG 
const loggerGenerator = require("../../../core/logger/logger") ;
const constants = require("../../../core/constants/constants");
const version = constants.technical.VERSION_1 ;
const  layerName = constants.technical.LAYER_SERVICE ;
const  servicerName = constants.technical.USER
const logger = loggerGenerator.getLoger(layerName,servicerName) ;

class User
  {


    constructor(){
   
      }

    async createUser(user) {

        // clean the description
        logger.info("Call createUser");

        // TO DO MOVE  in transformators
        if (processing.isNullOrEmpty(user.description)) {
            user.description = await this.cleanDescription(user.description,user)
        }

        const newUser = new UserModel(user);
        let resultUser = await newUser.save();

        // Create affilition
        await  affiliationService.createAffiliation({userId:resultUser._id,from:(user.affilition ?user.affilition.from :""), name:user.name, surname:user.surname}) ;

        const permissionLevels = [];
        user.state.forEach((element) => {
            if (element === 'employee') {
                permissionLevels.push(config.EMPLOYEE);

            } else if (element === 'employer') {
                permissionLevels.push(config.EMPLOYER);
            }
        });

 
        // Create role 
        const newRole = {'userId':resultUser._id, 'permissionLevel': permissionLevels};
        await roleService.createRole(newRole);

        // Inject dependance
        accountService.setGetUser(this.getUser);
        accountService.setUpdateUser(this.updateUser);
    
        // activation link
        await accountService.sendActivationLink(resultUser) ;

        return resultUser ;
    
    };


 
async  cleanDescription(description,user){

    // WE REMOVE PERSONNAL INFORMATION FROM  DESCRIPTION 
    // TODO  TRANSFORM IN MORE GENERIC FUNCTION AND MOVE IT  in core processing
    description = preprocessing.anonymizeText(user.surname, user.surname[0] + ".",description);
    description = preprocessing.anonymizeText(user.name, user.name[0], description);
    description = preprocessing.removePhoneNumber(description, " ");
    return description ;
}

    async getAllUsers (perPage, page, filterParams, pagination = true) {
        // delete the not used filterParams
        logger.info("Call getAllUsers ");

        delete filterParams.limit;
        delete filterParams.page;
        delete filterParams.lang;
        filterParams = formatFilterParams(filterParams,[]);
        if(filterParams.tags){
            filterParams.tags= { "$in" :filterParams.tags}
        }
        if(filterParams.phoneNumber){
            filterParams['phoneNumber.value'] = parseInt(filterParams.phoneNumber) ;
            delete filterParams.phoneNumber;
        }

        let result = !pagination ? await UserModel.find(filterParams).select('-__v').lean().exec()
            : await UserModel.find(filterParams).limit(perPage).skip(perPage * page).select('-__v').lean().exec();
        let length = await UserModel.find(filterParams).countDocuments();
        return {"users": result, "length": length};
    };
    
    async updateUser  (id, user, state = 1) {
        let result;
            let currentUser;
            logger.info("Call  updateUser ");
            if(state === 0){
                currentUser = await UserModel.findOne({'email.value': id}).exec();
                if(currentUser && !currentUser.valid){
                    currentUser = null;
                }
            }else{
                currentUser = await UserModel.findById(id).exec();
            }
        if (currentUser) {
            currentUser.set(user);
            result = await currentUser.save();
            result.toJSON();
            delete result._id;
            delete result.__v;
            delete result.password;
            }
            logger.info('update service user by id : ' + id);
        return result;
    };
    
    async getUser  (field, value)  {
        let user;
        logger.info("Call  getUser ");
        if (field === 'email') {
            user = await UserModel.findOne({"email.value": value}).select('-__v').lean().exec();
        } else if (field === 'phoneNumber') {
            user = await UserModel.findOne({"phoneNumber.value": value}).select('-__v').lean().exec();
        } else if (field === 'affiliation') {
            user = await UserModel.findOne({"affiliation.code": value}).select('-__v').lean().exec();
        } else if (field === "random_code_for_processes") {
            user = await UserModel.findOne({"random_code_for_processes.requestId": value}).select('-__v').lean().exec();
        } else {
            user = await UserModel.findById(value).select('-__v').lean().exec();
        }
        logger.info('get user service by  field : ' + field + ' and value :' + value);
        return user;
    };
    
    async getUserInitial(id){
        let user = await UserModel.findById(id).select('initial').lean().exec();
        return user.initial;
    };
    
    async getUserPhoneNumber (id){
        let user = await UserModel.findById(id).select('phoneNumber').lean().exec();
        return user.phoneNumber.value;
    };
    
    async getUserAnonymous   (field, value){
        let user;
        user = field === 'email' ? await UserModel.findOne({'email.value': value}).select('-phoneNumber -__v -email -MoneyAccount  -valid -state -password -validationToken -name -surname').exec()
                : await UserModel.findById(value).select('-phoneNumber -email -MoneyAccount -valid  -state -password -validationToken -name -surname -__v').lean().exec();
            if (user) {
                // compute age
                const diff_ms = Date.now() - user.birthday.getTime();
                const age_dt = new Date(diff_ms);
                user.age = Math.abs(age_dt.getUTCFullYear() - 1970);
                delete user.birthday;
            }
            return user;
    };
    
    async  getUserWhenAcquitted (field, value){
        let user;
        user = field === 'email' ? await UserModel.findOne({'email.value': value}).select(' -valid -birthday -state -password -validationToken -__v -phoneNumber.requestId').exec()
            : await UserModel.findById(value).select(' -valid  -state -password -validationToken -__v -phoneNumber.requestId').exec();
        if (user) {
            user = user.toJSON();
            const diff_ms = Date.now() - user.birthday.getTime();
            const age_dt = new Date(diff_ms);
            user.age = Math.abs(age_dt.getUTCFullYear() - 1970);
            delete user.birthday
        }
        return user;
    };
    

    
    async  deleteUser (id){
        let result;
        const user = await UserModel.findById(id).select('state -_id').exec();
        const userState = user.state;

        const resultRole = await RoleModel.deleteMany({'userId': id}).exec();
        if (resultRole) {
            result = await UserModel.deleteOne({'_id': id}).exec();
            return result.deletedCount > 0;
        } else {
            return false;
        }
    
    };
    
    async  updateRoleByUser (userId, Role){
        let result;
        const currentUser = await UserModel.findOne({'_id': userId}).exec();
        console.log("user "+currentUser.state) ;
        let roles = currentUser.state;
        if (!roles.includes(Role)) {
            roles.push(Role);
        }
        if (currentUser) {
            currentUser.set({'state': roles});
            result = await currentUser.save();
            return result;
        } else {
            return null;
        }
    };
    
    async searchUserByName (name) {
        let result;
        let output = [];
        let query = [{"name": new RegExp(".*" + name + ".*", "i")},
            {"surname": new RegExp(".*" + name + ".*", "i")}];
        result = await UserModel.find().or(query).limit(6).sort('name').select('_id name surname state').lean().exec();
        if (result && result.length && result.length > 0) {
            result.forEach(user => {
                let obj = {
                    id: user._id,
                    label: user.name + ' ' + user.surname,
                    state: user.state
                };
                output.push(obj);
            });
        }
        return output;
    };
    

  }


module.exports = new User();