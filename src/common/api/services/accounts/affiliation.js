// MODEL
const AffiliationModel = require('../../models/affiliation');

const userService = require('./user');
const companyService = require('./company');
const notificationService = require('../channels/notification');
// LOG 
const loggerGenerator = require("../../../core/logger/logger") ;
const constants = require("../../../core/constants/constants");
const version = constants.technical.VERSION_1 ;
const  layerName = constants.technical.LAYER_SERVICE ;
const  servicerName = constants.technical.USER
const logger = loggerGenerator.getLoger(layerName,servicerName) ;
class Affiliation  {

    constructor() {

    }

  async createAffiliation(affiliation) {
        //Add codeAffiliation
        affiliation.code = await  this.generateAffiliationCode (affiliation) ;
        const newAffiliation = new AffiliationModel(affiliation);
        let result = await newAffiliation.save();
        // INCREMENT COUNT IF THERE A THE FIELD  
        if(affiliation.from) {
            const  affiliateFrom = await AffiliationModel.findById(result._id) ;
            affiliateFrom.set({'count': affiliateFrom.count+1});
            result = await affiliateFrom.save() ;
            return result;
        }
        return result;
    };
    
   async getAllAffiliations(perPage, page) {
        let result = await AffiliationModel.find({}).limit(perPage).skip(perPage * page).select('-__v').lean().exec();
        return result;
    };
    
     async updateAffiliationByUser (userId, Affiliation) {
        let result;
        const currentAffiliation = await AffiliationModel.findOne({'userId': userId}).exec();
        let Affiliations = currentAffiliation.permissionLevel;
        if (!Affiliations.includes(Affiliation)) {
            Affiliations.push(Affiliation);
        }
        if (currentAffiliation) {
            currentAffiliation.set({'userId': userId, 'Affiliation': Affiliations});
            result = await currentAffiliation.save();
            return result;
        } else {
            return null;
        }
    };
    

    async deleteAffiliation(id){
        let result = await AffiliationModel.deleteOne({'_id': id}).exec();
        return result.deletedCount > 0;
    };



  async  generateAffiliationCode (newUser) {
        const uperBound = 9999;
        const newUserCodeAffiliation = newUser.name.substring(0, 2).toLowerCase() + newUser.surname.substring(0, 4).toLowerCase() + (Math.ceil(Math.random() * uperBound).toString())  ;
        return newUserCodeAffiliation ;
    }; 

}
            

module.exports =  new Affiliation();
