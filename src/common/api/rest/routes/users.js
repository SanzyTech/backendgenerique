const express = require('express');

const userController = require('../controllers/account/user');
const accountController = require('../controllers/account/account');

//const fileManagerController = require('../controllers/fileManager');
const notificationController = require('../controllers/channels/notification');
//const administratorController = require('../controllers/administrator');
const mailController = require('../controllers/mail');
const smsController = require('../controllers/sms');
const validationSecurity = require('../controllers/security/validation');
const permissionSecurity = require('../controllers/security/permission');
const sanitizeSecurity = require('../controllers/security/sanitize');

const router = express.Router();
const config = require('../../../../../config/environnement/config');

const ADMIN = config.permissionLevels.ADMIN;
const EMPLOYER = config.permissionLevels.EMPLOYER_USER;
const EMPLOYEE = config.permissionLevels.EMPLOYEE_USER;
const CONTROLLER = config.permissionLevels.CONTROLLER_USER;
const SUP_ADMIN = config.permissionLevels.SUP_ADMIN;
const ENTREPRISE = config.permissionLevels.ENTREPRISE_USER;

const busboy = require('connect-busboy');
const busboyBodyParser = require('busboy-body-parser');

router.get('/:id',
    [
        validationSecurity.validJWTNeeded,
        permissionSecurity.permissionLevelRequired([ADMIN, CONTROLLER, SUP_ADMIN]),
        userController.getUserById,
    ]);

router.get('/profile/me',
    [
        validationSecurity.validJWTNeeded,
        userController.getUserById
    ]);

router.post('/',
    [
        sanitizeSecurity.validate('createUser'),
        //businessUser.checkAge,
        userController.createUser
    ]);


    router.get('/:id/verify_account',
    [
        accountController.confirmCreate
    ]);


// TO DO TEST AND UPDATE
router.post('/option/withoutPassword',
    [//businessUser.isPasswordGenerated,
        sanitizeSecurity.validate('createUser'),
        //businessUser.checkAge,
        userController.createUserWithoutPassword
    ]);



router.post('/resetForgottenPassword/mail',
    mailController.sendMailForgottenPassword('UserService')
);

router.post('/resetForgottenPassword/mail2',
    mailController.sendMailForgottenPasswordRandomCode('UserService')
);

router.put('/changePassword',
    validationSecurity.validJWTNeeded,
    permissionSecurity.permissionLevelRequired([EMPLOYEE, EMPLOYER]),
    mailController.replaceForgottenPassword('UserService')
);

router.put('/changePassword2',
    mailController.replaceForgottenPasswordRandomCode('UserService')
);

router.post('/RandomCodeRequest/queryChangeMail',
    validationSecurity.validJWTNeeded,
    permissionSecurity.permissionLevelRequired([ADMIN, CONTROLLER, SUP_ADMIN, EMPLOYER, EMPLOYEE]),
    smsController.queryChangeMailAdress('UserService')
);

router.put('/RandomCodeRequest/changeMail',
    smsController.replaceEmailAdress('UserService')
);


router.put('/:id',
    [
        validationSecurity.validJWTNeeded,
        permissionSecurity.permissionLevelRequired([ADMIN, CONTROLLER, SUP_ADMIN]),
        userController.updateUser
    ]);

router.put('/profile/me',
    [
        validationSecurity.validJWTNeeded,
        permissionSecurity.permissionLevelRequired([EMPLOYER, EMPLOYEE, ADMIN, CONTROLLER, SUP_ADMIN]),
        userController.updateUser
    ]);
/*
router.put('/notify/me',
    validationSecurity.validJWTNeeded,
    permissionSecurity.permissionLevelRequired([EMPLOYEE, EMPLOYER]),
    employerController.updateNotify
);
*/


router.get('/',
    [
        validationSecurity.validJWTNeeded,
        permissionSecurity.permissionLevelRequired([ADMIN, CONTROLLER, SUP_ADMIN]),
        userController.getAllUsers
    ]);

router.delete('/:id',
    [
        validationSecurity.validJWTNeeded,
        permissionSecurity.permissionLevelRequired([ADMIN, SUP_ADMIN]),
        userController.deleteUser
    ]);

router.get('/:id/verify_account',
    [
        mailController.confirmCreate('UserService')
    ]);


module.exports = router;
