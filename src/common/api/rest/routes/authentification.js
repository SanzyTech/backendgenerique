const express = require('express');
const router = express.Router();
const authentificationMiddleware = require('../middleware/validators/authentification');
const authController = require('../controllers/security/authentification');


router.post('/login/:user',
    [
        // TODO REMOVE COMMENT
        //attackSecurity.attackLimiter('Too Many requests to Sign in. Your account has been blocked and you received an email' +
            //' to change your password.'),
        authentificationMiddleware.hasAuthValidFields,
        authentificationMiddleware.isPasswordAndUserMatch,
        authController.login,
    ]);

module.exports = router;
