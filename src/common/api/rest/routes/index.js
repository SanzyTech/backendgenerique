/** ********
 * index.js file (for routes)
 **********/
const path = require('path');
const fs = require('fs');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./../../../../../config/swagger.json');
const apiRoute = '/api/v1/';
const endPoints = [];
const express = require('express');

//configs
const config = require('../../../../../config/environnement/config');
const CLIENT = "client"

// Log 
const constants = require("../../../../../src/common/core/constants/constants")
const projectRouteName =  `../../../../${constants.business.APP_NAME.toLowerCase()}/api/rest/routes/`;

const  layerName = constants.technical.LAYER_ROUTE
const  servicerName = constants.technical.INDEX
const loggerGenerator = require("../../../../common/core/logger/logger")
const logger = loggerGenerator.getLoger(layerName,servicerName)


const init = (server) => {
  server.get('*', function (req, res, next) {
    logger.info('Request to: ' + req.originalUrl);
    return next();
  });
  // Load all the js file in this folder :
  // Set up routes
  fs.readdirSync(__dirname+"/"+projectRouteName).forEach(function (routeFile) {
      if (routeFile === 'index.js' || routeFile === 'resource.js') return;
    const routeFileCompletePath = path.join(__dirname+"/"+projectRouteName, routeFile);
    const stat = fs.statSync(routeFileCompletePath);
    if (stat && !stat.isDirectory()) {
      endPoints.push(routeFileCompletePath);
      logger.info("url "+projectRouteName);
      server.use(apiRoute + path.basename(routeFile, '.js'), require(projectRouteName+path.basename(routeFile, '.js')));
    }
  });

  fs.readdirSync(__dirname).forEach(function (routeFile) {
    if (routeFile === 'index.js' || routeFile === 'resource.js') return;
  const routeFileCompletePath = path.join(__dirname, routeFile);
  const stat = fs.statSync(routeFileCompletePath);
  if (stat && !stat.isDirectory()) {
    endPoints.push(routeFileCompletePath);

    server.use(apiRoute + path.basename(routeFile, '.js'), require('./' + path.basename(routeFile, '.js')));
  }
});
  // server.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));
    swaggerDocument.host = config.hostname === 'localhost' ? config.hostname + ':' + config.port : config.hostname;

  if(config.env !== './production'){
    server.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
  }

  // use to access css file
  server.use(express.static(path.join(__dirname, '/../common/pages')));
  // serving html pages
  server.use('/error', express.static(path.join(__dirname, '/../common/error/pages')));

    server.use('/.well-known/acme-challenge/LsS3QmXCpsyQB-vFFLZ2PErLHvyT4kbU07TywKVC_KI', function (req, res) {
        res.send('LsS3QmXCpsyQB-vFFLZ2PErLHvyT4kbU07TywKVC_KI.VZiBv_X7gC9fFFjUN-8QTPjJ4HcOggq_N0_MiqA2nS4');
    });

  server.post('/report-violation', function (req, res) {
    res.status(200).json({'message': 'XSS ATTACK !', 'warning': 'Origin not registered in CSP'});
  });

  server.get('*', function (req, res) {
    res.set('Cache-control', 'public, max-age=200');
    res.sendFile(path.join(__dirname, '../react-app/build/index.html'));
  });
};

module.exports = {
  init: init,
};
