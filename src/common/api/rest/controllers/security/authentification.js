
// SERVICE 
const userService = require('../../../services/accounts/user');

const roleService = require('../../../services/accounts/role');
const bcrypt = require('bcrypt');
const error_processing =  require('../../../../core/error/error');

// CONFIGS

const config = require('../../../../../../config/environnement/config');
const no_mail = config.no_mail;
const EMPLOYER = config.permissionLevels.EMPLOYER_USER;
const EMPLOYEE = config.permissionLevels.EMPLOYEE_USER;
const jwtSecret = config.jwt_secret;
const jwt = require('jsonwebtoken');
const saltRounds = config.sr;
const CLIENT = "client"
// Log 
const constants = require("../../../../core/constants/constants")
const  layerName = constants.technical.LAYER_CONTROLLER
const  servicerName = constants.technical.PARTICULAR
const loggerGenerator = require("../../../../core/logger/logger")
const logger = loggerGenerator.getLoger(layerName,servicerName)

// token check the account state (verified email)
const login = async (req, res) => {
    let lang = req.query.lang ? req.query.lang : 'en';
    try {
        const token = jwt.sign(req.body, jwtSecret, {expiresIn: config.jwt_expiration_in_seconds});
        logger.info('Call of login ');
     
        await userService .updateUser(req.body.userId, {"lastConnection": Date.now()});

            logger.info(req.body);
        return res.status(201).json({
            'data': {
                accessToken: token
            },
        });
    } catch (e) {
        logger.info(e.message);
        const err = new error_processing.ServerError(e, lang);
        return res.status(500).json({
            'message': error_processing.process(err)
        });
    }
};

module.exports = {
    login: login,
};
