// libraries
const fs = require('fs');
const path = require('path');
const {validationResult} = require('express-validator');
const crypto_string = require('crypto-random-string');
const error_processing = require('../../../../core/error/error');
const bcrypt = require('bcrypt');
const processing = require('../../../../core/processing/processing');
// services
const userService = require('../../../services/accounts/user');
const locationService = require('../../../services/accounts/location');
const roleService = require('../../../services/accounts/role');
const emailService = require('../../../services/channels/email');
const smsService = require('../../../services/channels/sms');
const smsFunctions = require('../sms');


//configs
const config = require('../../../../../../config/environnement/config');
const no_mail = config.no_mail;
const EMPLOYER = config.permissionLevels.EMPLOYER_USER;
const EMPLOYEE = config.permissionLevels.EMPLOYEE_USER;
const saltRounds = config.sr;
const CLIENT = "client"
// Log 
const constants = require("../../../../core/constants/constants")
const  layerName = constants.technical.LAYER_CONTROLLER
const  servicerName = constants.technical.USER
const loggerGenerator = require("../../../../core/logger/logger")
const logger = loggerGenerator.getLoger(layerName,servicerName)

 class Account  {

    constructor(){
      }

async confirmCreate  (req, res)  {

        logger.info('confirmCreate');
        logger.info('Controller for email confirmation !');
        let mailLink = config.env === './dev' ?
            'http://' + config.hostname + ':' + config.port :  'https://' + config.hostname;
        try {
            let currentAccount;
            currentAccount = await userService.getUser('id', req.params.id);
            if (currentAccount) {
                if (currentAccount.valid === true) {
                    logger.info("account already checked by sms or by email");
                    mailLink = mailLink + '/fr/login?register=1' ;
                    res.redirect(mailLink);
                } else {
                    //logger.info(currentAccount);
                    if (currentAccount.validationToken.token === req.query.token) {
                        currentAccount.valid = true;
                        currentAccount.email.valid = true;
  
                        await userService.updateUser(req.params.id, currentAccount);
                        
                        logger.info('The account has been activated. Registration completed for ' + req.params.id);
                        mailLink = config.env === './production' ? 'https://www.' + config.hostname : mailLink;
                        mailLink = mailLink + '/fr/login?register=2' ;
                        res.redirect(mailLink);
                    } else {
                        logger.info("invalid url sorry");
                        mailLink = mailLink + '/fr/login?register=3' ;
                        res.redirect(mailLink);
                    }
                }
            } else {
                logger.info('It seems this account does not exist. Please register');
                mailLink = mailLink + '/fr/login?register=4' ;
                res.redirect(mailLink);
            }
        } catch (error) {
            logger.info(error);
            mailLink = mailLink + '/fr/login?register=5' ;
            res.redirect(mailLink);
        }
    
};

    
    }

module.exports = new Account()

