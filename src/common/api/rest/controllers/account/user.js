// libraries
const fs = require('fs');
const path = require('path');
const {validationResult} = require('express-validator');
const crypto_string = require('crypto-random-string');
const error_processing = require('../../../../core/error/error');
const bcrypt = require('bcrypt');
const processing = require('../../../../core/processing/processing');
// services
const userService = require('../../../services/accounts/user');
const locationService = require('../../../services/accounts/location');
const roleService = require('../../../services/accounts/role');
const emailService = require('../../../services/channels/email');
const smsService = require('../../../services/channels/sms');
const smsFunctions = require('../sms');


//configs
const config = require('../../../../../../config/environnement/config');
const no_mail = config.no_mail;
const EMPLOYER = config.permissionLevels.EMPLOYER_USER;
const EMPLOYEE = config.permissionLevels.EMPLOYEE_USER;
const saltRounds = config.sr;
const CLIENT = "client"
// Log 
const constants = require("../../../../core/constants/constants")
const  layerName = constants.technical.LAYER_CONTROLLER
const  servicerName = constants.technical.USER
const loggerGenerator = require("../../../../core/logger/logger")
const logger = loggerGenerator.getLoger(layerName,servicerName)

 class User  {

    constructor(){
      }
    async  getAllUsers(req, res) {
    const lang = req.query.lang ? req.query.lang : 'en';
    let message;
    logger.info("Call getAllUsers") ;
    try {
        let limit = req.query.limit && req.query.limit <= 100 ? parseInt(req.query.limit) : 10;
        let page = 0;
        if (req.query) {
            if (req.query.page) {
                req.query.page = parseInt(req.query.page);
                page = Number.isInteger(req.query.page) ? req.query.page : 0;
            }
        }
      const users = await userService.getAllUsers(limit, page, req.query);
        message = lang === "fr" ? "Les utilisateurs sont" : "there are users";
        return res.status(200).json({
            'message': message,
            'data': users
        });


    } catch (e) {
        logger.warning(e.message);
        const err = new error_processing.ServerError(e, lang);
        err.process()
        return res.status(500).json({
            'message': error_processing.process(err)
        });
    }
};

async verifyPhoneNumber(req, res)  {
        logger.info('Controller to Verify phoneNumber !');
        let currentAccount =  await userService.getUser('id', req.jwt.userId);
        let userId = req.jwt.userId;
        let message;
        try {
            if (currentAccount) {
                if (new Date() <= new Date(currentAccount.phoneNumber.requestIdExpired)) {
                    logger.info(currentAccount.phoneNumber.requestId);
                    if (Number(req.query.pin) === currentAccount.phoneNumber.requestId) {
                        currentAccount.phoneNumber.valid = true;
                        await userService.updateUser(userId,currentAccount);
                        message = req.query.lang === 'fr'  ? "numéro verifié avec succès !" :
                            "phoneNumber successful verified";
                        return res.status(200).json({
                            'message':  message
                        });
                    } else {
                        message = req.query.lang ? 'Code incorrect' : 'the code is wrong';
                        return res.status(400).json({
                            'message':  message
                        });
                    }
                } else {
                    message = req.query.lang ? "Le code a expiré, essayez à nouveau s'il vous plait"
                        : "the code expired, please try again";
                    return res.status(400).json({
                        'message':  message
                    });
                }
            } else {
                message = req.query.lang === 'fr' ? "Ce compte n\'existe pas. Veuillez créer un compte d\'abord" :
                    "It seems this account does not exist. Please register";
                logger.info('It seems this account does not exist. Please register');
                return res.status(404).json({
                    'message': message
                });
            }
        } catch (e) {
            logger.info(e.message);
            const err = new error_processing.BusinessError(" ", "", 500, "undefined", req.query.lang);
            return res.status(500).json({
                'message': error_processing.process(err)
            });
        }
};

async createUser (req, res) {

    const lang = processing.getLanguage(req.query.lang);

    let message;
    try {
        
        if (req.body.locationPlaceId) {
            const geoLocation = await locationService.getLocationByPlaceId(req.body.locationPlaceId);
            req.body.geo_coordinates = {
                "longitude": geoLocation.location.long,
                "latitude": geoLocation.location.lat
            };
            delete req.body.locationPlaceId;
            logger.info(req.body.geo_coordinates);
            
        }

            req.body.name = req.body.name.replace("'", "");
            req.body.surname = req.body.surname.replace("'", "");
            // create the validation with email
            req.body.validationToken = {'token': crypto_string({length: 32}), 'date': Date.now()};
            const hash = await bcrypt.genSalt(Number(saltRounds));
            req.body.password = await bcrypt.hash(req.body.password, hash);
            // create the user service
            let tmp = "237" + req.body.phoneNumber.value.toString();
            req.body.phoneNumber.value = Number(tmp);
            // if this property affiliation doesn't exist , we create it.
            req.body.affiliation = !req.body.affiliation ? {}: req.body.affiliation ;

            const user = await userService.createUser(req.body)
                return res.status(200).json({
                    'data': {'id': user._id}
                });
            
        }
     catch (e) {

        logger.info('Attemps to create an account :  ' + req.body.phoneNumber.value);
        logger.info(req.body);
        logger.info(e.message);
        const err = new error_processing.ServerError(e, lang);
        const message = await err.process(false) ;
        return res.status(500).json({
            'message': "error while creating user "+message 
        });
    }
};


async  createUserWithoutPassword (req, res) {
    const lang = req.query.lang ? req.query.lang : 'en';
    let sms_code;
    let dataIn;
    let message;
    try {

        /*
        if (req.body.email.value === "") {
            delete req.body.email.value;
        }
        const errors = validationResult(req);
        // I check whether there are errors or not
        if (!errors.isEmpty()) {
            const errorList = errors.array();
            res.status(400).json({message: errorList[0].msg});
            return;
        }
        let validPassword = await processing.securePassword(req.body.password);
        if (validPassword.valid) {
            logger.info('Call create user controller without password');
            // security control for business
            // check the state
            if (req.body.state.includes('admin')) {
                if (!req.body.email.value.endsWith('@jobaas.cm')) {
                    message = lang === 'fr' ? "Vous n'avez pas les droits d'administrateur" : 'Bad request you cannot be set admin';
                    return res.status(400).json({
                        'message': message
                    });

                }
            }

            req.body.name = req.body.name.replace("'", "");
            req.body.surname = req.body.surname.replace("'", "");
            // create the validation with email
            req.body.validationToken = {'token': crypto_string({length: 32}), 'date': Date.now()};
            const hash = await bcrypt.genSalt(Number(saltRounds));
            req.body.password = await bcrypt.hash(req.body.password, hash);
            // create the user service
            let tmp = "237" + req.body.phoneNumber.value.toString();
            req.body.phoneNumber.value = Number(tmp);
            req.body.creation_is_assisted = true;
            const user = await userService.createUser(req.body);
            if (user) {
                const permissionLevels = [];
                req.body.state.forEach((element) => {
                    if (element === 'employee') {
                        permissionLevels.push(EMPLOYEE);

                    } else if (element === 'employer') {
                        permissionLevels.push(EMPLOYER);
                    }
                });
                await employerStatService.createStatsEmployer({userId: user._id});
                await employeeStatService.createStatsEmployee({userId: user._id});
                logger.info('permissions ' + permissionLevels);
                const newRole = {'userId': user._id, 'permissionLevel': permissionLevels};
                await roleService.createRole(newRole);
                const newMarketingView = {'userId': user._id, 'origin': req.body.origin};
                await marketingService.createMarketing(newMarketingView);
                let mailLink;
                // We define mailLink
                mailLink = config.env === './dev' ? 'http://' + config.hostname + ':' + config.port : 'https://' + config.hostname;
                //for   prod host we must add  wwww before the link
                mailLink = config.env === './production' ? 'https://www.' + config.hostname : mailLink;
                logger.info('mailLink : ' + mailLink + ' hostname : ' + config.hostname);
                const apiLink = mailLink + '/api/v1/user/' + user.id + '/verify_account?lang=fr&token=' + req.body.validationToken.token;
                logger.info('apiLink : ' + apiLink);
                let email = fs.readFileSync(path.join(__dirname, '/../common/mails/registration_fr.html'), 'utf8');
                email = email.replace('#name',    req.body.surname);
                email = email.replace('#mail',    req.body.email.value);
                email = email.replace('#password',  req.body.rawPassword);
                email = email.replace(/#lien/g, apiLink);
                //We  send a SMS for phonenumber's validation
                sms_code = await smsFunctions.generateSmsCode("user", user._id);
                const linkNumberValidation = mailLink + '/api/v1/user/' + user._id + '/verify_phonenumber?lang=fr&pin=' + sms_code;
                logger.info("validation number " + linkNumberValidation);
                co;
                // TODO REFACTORING
                dataIn =    {
                    "text": `Bienvenue sur Jobaas  ${req.body.surname}. \nAfin de vous connecter, utilisez ce mot de passe: ${req.body.rawPassword}\nPour activer votre compte, cliquer sur le lien suivant: ${linkNumberValidation}\n Pour nous contacter:  ${config.phonenumbers}. A très bientôt.`
                    , "to": req.body.phoneNumber.value
                };
                //TO DO PUT DEV AFTER
                if (Number(no_mail) === 0) {
                    logger.info('after the no mail condition');
                    if (req.body.email.value && req.body.email.value !== "") {
                        await email.nodemailer_mailgun_sender({
                            "from": 'Jobaas <no_reply@jobaas.cm>',
                            "to": user.email.value,
                            "subject": 'Confirmation inscription Jobaas',
                            "html": email
                        });
                    }
                    await smsService.send_notification(dataIn);
                }
                return res.status(200).json({
                    'data': {'id': user._id}
                });
            }
        } else {
            message = lang === 'fr' ? 'Mot de passe invalide. Il doit comporter 8 caractères au minimum' +
                'parmi lesquels au moins, un chiffre, une majuscule un miniscule et un caractère spécial parmi [!%#_?@]' :
                'Invalid password. there should be at least 8 characters ' +
                'and at least one number, one capital and lower character, ' +
                'and one no alphanumerique character among [!%#_?@]';
            logger.info(message);
            return res.status(400).json({
                'message': message
            });
        }*/
    } catch (e) {
        logger.info(e.message);
        const err = new error_processing.ServerError(e, lang);
        return res.status(500).json({
            'message': error_processing.process(err)
        });
    }
};

async getUserById  (req, res){
  let meanRatingEmployer = null;
  let meanRatingEmployee = null;
    let message;
    const lang = req.query.lang ? req.query.lang : 'en';
  try {
      let userId = req.params.id ? req.params.id : req.jwt.userId;
      //logger.info(userId);
    const user = await userService.getUser('id', userId);
    if (user) {
        delete user.validationToken;
        delete user.password;
        logger.info("state : " + user.state);
      return res.status(200).json({
        'data': {
          "user": user
        },
      });
    } else {

        message = lang === "fr" ? 'Pas de user ayant cet id ' : 'no user found by id ';
        return res.status(404).json({
            'message': message
        });

    }
  } catch (e) {
      logger.info(e.message);
      const err = new error_processing.ServerError(e, lang);
      return res.status(500).json({
          'message': error_processing.process(err)
      });
  }
};


async updateUser  (req, res) {
    logger.info('update user process');
    let message;
    const lang = req.query.lang ? req.query.lang : 'en';
  try {
    let user;
      if (req.body.password) {
          const hash = await bcrypt.genSalt(Number(saltRounds));
          if (processing.securePassword(req.body.password)) {
              req.body.password = await bcrypt.hash(req.body.password, hash);
          } else {
              message = lang === 'fr' ? 'Mot de passe invalide. Il doit comporter 8 caractères au minimum' +
                  'parmi lesquels au moins, un chiffre, une majuscule un miniscule et un caractère spécial parmi [!%#_?@]' :
                  'Invalid password. there should be at least 8 characters ' +
                  'and at least one number, one capital and lower character, ' +
                  'and one no alphanumerique character among [!%#_?@]';
              return res.status(400).json({
                  'message': message
              });
          }
      }
    let userId;

    if (req.params.id) {
      userId = req.params.id;
    } else {
      userId = req.jwt.userId;
      if(req.body.state){
          logger.info('you are not allowed to change the state : ' + userId);
          message = lang === "fr" ? "Vous n'avez pas les droits de modifier cette information" : "You are not allowed to change the state";
          return res.status(403).json({
              'message': message
          });
      }
    }
      user = await userService.updateUser(userId, req.body);
    if (!user) {
        message = lang === "fr" ? "Pas d'utilisateur trouvé avec l'id " + userId : 'user not found  id ' + userId;
        logger.info('user not found  id ' + userId);
        return res.status(404).json({
            'message': message
        });
    }
      message = lang === "fr" ? "Mise à jour du compte utilisateur" : 'user was updated';
      logger.info('user was updated');
    // logger.info(user);
    return res.status(200).json({
      'message': message,
      'data': user,
    });
  } catch (e) {
      logger.info(e.message);
      const err = new error_processing.ServerError(e, lang);
      return res.status(500).json({
          'message': error_processing.process(err)
      });
  }
};

async deleteUser  (req, res)  {
    const lang = req.query.lang ? req.query.lang : 'en';
    let message;
  try {
    let userId;
    if (req.params.id) {
      userId = req.params.id;
    } else {
      userId = req.jwt.userId;
    }

    const state = await userService.deleteUser(userId);
    if (state) {
        message = lang === "fr" ? "Suppression du compte utilisateur avec l'id: " + userId : "the user was deleted by id : " + userId;
        logger.info('the user was deleted by id : ' + userId);
      return res.status(200).json({
        'message': message
      });
    } else {

        message = lang === "fr" ? "Aucun compte utilisateur n'a été trouvé" : 'No user was found';
        logger.info(message);
        return res.status(404).json({
            'message': message
        });
    }
  } catch (e) {
      logger.info(e.message);
      const err = new error_processing.ServerError(e, lang);
      return res.status(500).json({
          'message': error_processing.process(err)
      });
  }
};

async getBestJobers  (req, res) {
    let lang = req.query.lang ? req.query.lang : 'en';

    try {
        const filterParameters = {};
        const bestJobers = await userService.getBestJobers(filterParameters);
        return res.status(200).json({
            'data': bestJobers
        });
    } catch (e) {
        logger.info(e.message);
        const err = new error_processing.ServerError(e, lang);
        return res.status(500).json({
            'message': error_processing.process(err)
        });
    }
};

async getBestJober  (req, res)  {
    let lang = req.query.lang ? req.query.lang : 'en';
    try {
        const filterParameters = {id: req.jwt.userId};
        const bestJober = await userService.getBestJober(filterParameters);
        return res.status(200).json({
            'data': bestJober
        });
    } catch (e) {
        logger.info(e.message);
        const err = new error_processing.ServerError(e, lang);
        return res.status(500).json({
            'message': error_processing.process(err)
        });
    }
};

    
    }

module.exports = new User()