const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const roleSchema = new Schema(
    {
        userId: {
            type: String,
            trim: true,
            required: [true, "userId field is required to create a role "],
        },
        permissionLevel: {
            type: [Number],
            default: [1],
        },
        createdAt: {
            type: Date,
            default: Date.now()
        }
    },
);

module.exports = mongoose.model('Role', roleSchema);
