const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const resourceSchema = new Schema(
    {
        name: {
            type:String,
            required: [true, "resource id field is required to create a resource "]
        },
        description: {
            type: String,
            trim: true
        }
    },
);

module.exports = mongoose.model('Resource', resourceSchema );
