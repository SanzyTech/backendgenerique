const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const permissionSchema = new Schema(
    {
        role_id: {
            type: String,
            trim: true,
            required: [true, "role id field is required to create resource "],
            index: true,
        },
        resource_id: {
            type: String,
            ref: 'resource',
            required: [true, "evaluator's id field is required to create a ressource "]
        },
        comment: {
            type: String,
            trim: true
        }
    },
);

module.exports = mongoose.model('Permission', permissionSchema );
