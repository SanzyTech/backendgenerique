const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const tags = require('../../core/types/enum').tags;
const schoolLevel = require('../../core/types/enum').schoolLevel;
const driver_category = require('../../core/types/enum').driver_category;
const vehicles = require('../../core/types/enum').vehicles;
const origin_tags = require('../../core/types/enum').origin_tags;
const type_client = require("../../core/types/enum").type_client;
const language_level = require('../../core/types/enum').language_level;
const language = require('../../core/types/enum').language;
const type = require('../../core/types/type');

// LOG 
const loggerGenerator = require("../../core/logger/logger") ;
const constants = require("../../core/constants/constants");
const version = constants.technical.VERSION_1 ;
const  layerName = constants.technical.LAYER_MODEL;
const  servicerName = constants.technical.AFFILIATION
const logger = loggerGenerator.getLoger(layerName,servicerName) ;

const affiliationSchema = new Schema(
    {
    
        registrationDate: {
            type: Date,
            default: Date.now()
        },
        code: {
            type: String
        },
        from: {
            type: String
        },
        count: {
            type: Number,
            required: false,
            default: 0
        },
        updateAt: {
            type: Date
        }
        
    });

const affiliation = mongoose.model('affiliation', affiliationSchema);
affiliation .syncIndexes().then(function () {
    logger.info("sync index done for affiliation ");
});

module.exports = affiliation ;
