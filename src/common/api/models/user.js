const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const tags = require('../../core/types/enum').tags;
const user_states = require('../../core/types/enum').user_states;
const schoolLevel = require('../../core/types/enum').schoolLevel;
const driver_category = require('../../core/types/enum').driver_category;
const vehicles = require('../../core/types/enum').vehicles;
const origin_tags = require('../../core/types/enum').origin_tags;
const type_client = require("../../core/types/enum").type_client;
const language_level = require('../../core/types/enum').language_level;
const language = require('../../core/types/enum').language;
const type = require('../../core/types/type');

// LOG 
const loggerGenerator = require("../../core/logger/logger") ;
const constants = require("../../core/constants/constants");
const version = constants.technical.VERSION_1 ;
const  layerName = constants.technical.LAYER_MODEL;
const  servicerName = constants.technical.USER
const logger = loggerGenerator.getLoger(layerName,servicerName) ;

const userSchema = new Schema(
    {
        name: {
            type: String,
            trim: true,
            uppercase: true,
            required: [true, "name field is required to create a user account "],
            index: true,
        },
        profession: {
            type: String,
            trim: true,
            uppercase: true,
            index: true,
            required: [true, "profession field is required to create a user account "]
        },
        surname: {
            type: String,
            trim: true,
            required: [true, "surname field is required to create a user account "],
        },
        description: {
            type: String
        },
        gender: {
            type: String,
            required: [true, "gender field is required to create a user account "],
            enum: ["Man", "Woman"]
        },
        MoneyAccount: {
            type: [Number]
        },
        phoneNumber: {
            value: {
                type: Number,
                unique: true,
                required: [true, "phoneNumber field is required to create a user account "],
                trim: true,
                index: true,
                sparse: true
            },
            valid: {
                type: Boolean,
                default: false
            },
            requestId: {
                type: Number
            },
            requestIdExpired: {
                type: String
            }
        },
        email: {
            value: {
                type: String,
                unique: true,
                lowerCase: true,
                trim: true,
                index: true,
                sparse: true
            },
            valid: {
                type: Boolean,
                default: false
            }
        },
        valid: {
            type: Boolean,
            default: false
        },
        registrationDate: {
            type: Date,
            default: Date.now()
        },
        schoolLevel: {
            level: {
                type: String,
                enum: schoolLevel
            },
            diplomaYear: {
                type: Number
            },
            url: {
                type: String
            },
            valid: {
                type: Boolean,
                default: false
            }
        },
            profilePic: {
            url: {
                type: String
            },
            valid: {
                type: Boolean,
                default: false
            }
        },
        identityCard: {
            url: {
                type: String
            },
            valid: {
                type: Boolean,
                default: false
            }
        },
        cv: {
            url: {
                type: String
            },
            valid: {
                type: Boolean,
                default: false
            }
        },
        password: {
            type: String,
            required: [true, "password field is required to create a user account "]
        },
        random_code_for_processes :type.random_code_for_processes ,
        skills: {
            type: [String]
        },
        country: {
            type: String,
            required: [true, "country field is required to create a user account "],
            lowerCase: true
        },
        town: {
            type: String,
            required: [true, "town field is required to create a user account "],
            lowerCase: true
        },
        street: {
            type: String,
            required: [true, "street field is required to create a user account "],
            lowercase: true,
        },
        geo_coordinates: type.geo_coordinates,
        referenceStreet: {
            type: String
        },
        language: {
            type: [
                {
                    value: {
                        type: String,
                        enum: language
                    },
                    level: {
                        type: String,
                        enum: language_level
                    }
                }
            ],
            default: []
        },
        software: {
            type: [String],
            default: []
        },
        driver_permit: {
            vehicle: {
                type: String,
                enum: vehicles
            },
            url: {
                type: String
            },
            date: {
                type: Date,
            },
            category: {
                type: String,
                enum: driver_category
            },
            verified: {
                type: Boolean,
                default: false,
            }
        },
        state: {
            type: [String],
            trim: true,
            enum: user_states,
            default: []
        },
        birthday: {
            type: Date,
            required: [true, "birthday field is required to create a user account "]
        },
        validationToken: {
            token: String,
            date: Date
        },
        tags: {
            type: [String],
            trim: true,
            default: [],
            enum: tags
        },
        emailUsed: {
            type: Boolean,
            default: true
        },
        origin: {
            type: String,
            enum: origin_tags
        }
        ,
        initial: {
            type: String
        },
        updateAt: {
            type: Date
        },
        lastConnection: {
            type: Date
        },
        creation_is_assisted : {
                type: Boolean,
                default: false
        },
        is_notified: {
            type: Boolean,
            default: true
        }
    });

userSchema.pre('save', function (next) {
    if (this.isNew) {
        if (!this.profilePic.url) {
            if (this.gender === "Man") {
                this.profilePic.url = 'https://res.cloudinary.com/jobaas-files/image/upload/v1596616147/jobaas/man_y16ogs.png';
            } else {
                this.profilePic.url = 'https://res.cloudinary.com/jobaas-files/image/upload/v1596616151/jobaas/female_sr4iq1.png';
            }
        }
    } else {
        if (this.isModified('phoneNumber.value')) {
            this.phoneNumber.valid = false;
        }
        if (this.isModified('profilePic.url') && !this.isModified('profilePic.valid')) {
            this.profilePic.valid = false;
        }
        if (this.isModified('cv.url') && !this.isModified('cv.valid')) {
            this.cv.valid = false;
        }
        if (this.isModified('driver_permit') && !this.isModified('driver_permit.verified')) {
            this.driver_permit.verified = false;
        }
        if (this.isModified('schoolLevel') && !this.isModified('schoolLevel.valid')) {
            this.schoolLevel.valid = false;
        }
    }
    this.street = this.street.toLowerCase();
    this.street = this.street.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    this.town = this.town.toLowerCase();
    this.town = this.town.replace("é", "e");
    this.town = this.town.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    this.street = this.street.replace('&#x27', '');
    this.updateAt = Date.now();
    this.name = this.name.replace("&#X27", "");
    this.surname = this.surname.replace("&#X27", "");
    if (this.profession) {
        this.profession = this.profession.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
    }
    let tmpName = this.name.split(' ');
    let tmpSurname = this.surname ? this.surname.split(' ') : [];
    let initialName = '';
    let initialSurname = '';
    for (let name of tmpName) {
        initialName = initialName + name[0].toUpperCase();
    }
    for (let surname of tmpSurname) {
        initialSurname = initialSurname + surname[0].toUpperCase();
    }
    this.initial = initialName + initialSurname;
    next();
});

user = mongoose.model('user', userSchema);
user.syncIndexes().then(function () {
    logger.info("sync index done for user");
});

module.exports = user;
