const reasons = require('./enum').reasons_random_code;

const geo_coordinates =    {
    longitude: {
        type: Number,
        required: false
    },
    latitude: {
        type: Number,
        required: false
    }
};



const random_code_for_processes = {
    type: [
        {
            value: {
                type:Number,
                default: null,
            },
            generatedAt: {
                type:Date
            },
            cause: {
                type:String,
                enum: reasons
            },
            requestId: {
                type: String
            }
        }
    ],
    default: []
};

module.exports = {
    geo_coordinates: geo_coordinates,
    random_code_for_processes:random_code_for_processes
};