//
const config = require('../../../../config/environnement/config');

//enum
const stateJob = ["cancelled", "in progress", "completed", "created", "validated", "rejected", "expired"];
const languageLevel = ['beginner', 'intermediate', 'high', 'professional', 'maternal'];
const schoolLevel = ['6e', '5e', '4e', 'bepc', '2nd', 'probatoire', 'bac', 'bac+1', 'bac+2', 'bac+3', 'bac+4', 'bac+5', 'bac+6'];
const tags = ['Art', 'Education', 'House', 'Delivery',
    'Events', 'Commercial', 'Distribution', 'Transport', 'Technical_support', 'Animals', 'Beauty_bodycare',
    'Sport', 'Health', 'Fashion', 'Web_service', 'Others', 'Network_multimedia',
    'Relocation', 'Office_service', 'Hotel_catering'];
const tagsFr = ['Art', 'Education/Scolaire', 'Entretien Maison', 'Livraison',
    'Evènement', 'Commercial', 'Distribution', 'Transport', 'Support technique', 'Entretien des animaux', 'Beauté et soins',
    'Sport', 'Santé', 'Mode', 'Services web', 'Autres', 'Marketing digital',
    'Déménagement', 'Bureautique', 'Hôtellerie & traiteur'];
const tagsSchool = ['Education',
    'Events', 'Commercial', 'Technical_support', 'Health', 'Web_service', 'Network_multimedia',
    'Office_service', 'Hotel_catering'];
const origin = ["Friends", "Instagram", "Facebook", "Whatsapp", "LinkedIn", "Youtube", "Twitter", "Video", "Other", "Taxi", "Influencer","Sms","School_or_training_center"];
const type_event = ["other", "transaction", "rappel_job", "evaluation", "fileManager", "cancel_contract", "reject_application",
    "affiliation", "application", "litigation", "recommendation", "job_subscription", "task_controller", "save_application", "cancel_job", "new_job", "job_validation", "change_role", "job_expired"];
const driver_category = ['A', 'B', 'C', 'D', 'E', 'AM', 'A1', 'A2', 'B1', 'BE', 'C1E', 'CE', 'D1', 'D1E'];
const vehicles = ['car', 'bike', 'bus'];
const schoolLevelAdmin = ['bac+4', 'bac+5', 'bac+6'];
const stateContract = ['begin', 'middle', 'failed', 'closed', 'paid'];
const stateApplication = ['done', 'cancelled', 'validated', 'rejected', 'viewed'];
const stateLitigation = ["resolved", "in progress", "cancelled"];
const language = ['English', 'French', 'Italian', 'Spanish', 'Chinese', 'German'];
const file_tags = ['identity', 'profilePic', 'administrative', 'skill', 'litigation', 'other', 'schoolLevel', 'driver_permit', 'job','cv', 'logo'];
const reasons_random_code = ["change_password", "change_mail_adress"];
const type_client = ['new', 'regular', 'cold', 'lost'];
const type_prospect = ['prospect', 'hot_prospect'];
const user_states = ['employee', 'hr'];
const ADMIN = config.permissionLevels.ADMIN;
const CONTROLLER = config.permissionLevels.CONTROLLER_USER;
const SUP_ADMIN = config.permissionLevels.SUP_ADMIN;
const adminRights = [ADMIN, CONTROLLER, SUP_ADMIN];

const tag_list = {
    "key": tags,
    "fr": ['Art', 'Education/Scolaire', 'Entretien Maison', 'Livraison',
        'Evènement', 'Commercial', 'Distribution', 'Transport', 'Support technique', 'Entretien des animaux', 'Beauté et soins',
        'Sport', 'Santé', 'Mode', 'Services web', 'Autres', 'Marketing digital',
        'Déménagement', 'Bureautique', 'Hôtellerie & traiteur']
};

const bdd_to_tag = (entry, lang) => {
    let tagList;
    tagList = lang === 'fr' ? tag_list.fr : tag_list.key;
    let result = [];
    for (let i = 0; i < entry.length; i++) {
        let index = tag_list.key.indexOf(entry[i]);
        result.push(tagList[index]);
    }
    return result;
};

module.exports = {
    stateJob: stateJob,
    tags: tags,
    bdd_to_tag: bdd_to_tag,
    schoolLevel: schoolLevel,
    driver_category: driver_category,
    vehicles: vehicles,
    schoolLevelAdmin: schoolLevelAdmin,
    stateApplication: stateApplication,
    stateLitigation: stateLitigation,
    adminRights: adminRights,
    tagsSchool: tagsSchool,
    type_event: type_event,
    stateContract: stateContract,
    origin_tags: origin,
    file_tags: file_tags,
    language_level: languageLevel,
    language: language,
    tagsFr: tagsFr,
    reasons_random_code: reasons_random_code,
    user_states:user_states,
    type_client: type_client,
    type_prospect: type_prospect
};
