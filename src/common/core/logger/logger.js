const { createLogger, format, transports } = require("winston");
// THIS CLASS AIMS TO MANAGE LOG
class Logger {
    constructor(){
      
      }
      getLoger(layer,name){

        let logger = createLogger({
          format: format.combine(format.timestamp(), format.json()),
          defaultMeta: {
            layer:layer,
            name:name,
          },
          transports: [new transports.Console({})],
        });

        return logger

      }


      getLoggerService(){

      }


      getLoggerController(){

    }
    

}


module.exports = new Logger();
