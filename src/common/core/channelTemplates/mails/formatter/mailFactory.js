const fs = require('fs');
const path = require('path');
const REGISTRATION_TEMPLATE = '../template/fr/registration_fr.html';
const ALERTE_TEMPLATE = '../template/fr/alerte_fr.html';

const FOOTER_TEMPLATE = '../template/fr/footer.html';
const constants = require('../../../constants/constants');
const processing = require('../../../processing/processing');
const mail = require('../../../../api/services/channels/email');




  class MailTemplate {
    constructor() {
      this.bodyUrl = '' ;
      this.mail='';
      this.subject='' ;
      this.replacedWords = new Map();

    }


    readBody(){
        // WE REPLACE APP_NAME AND LOGO WHENEVER WE SEE IT
        this.mail = "" ;
      
        this.mail = fs.readFileSync(path.join(__dirname, this.bodyUrl), 'utf8');

        this.appendReplacement(constants.technical.TEMPLATE_MARKER_APP_NAME, constants.business.APP_NAME) ;

        this.appendReplacement(constants.technical.TEMPLATE_MARKER_URL_LOGO, constants.business.URL_LOGO) ;

    
    }

    setFooter(footerUrl = FOOTER_TEMPLATE) {

        const footer  = fs.readFileSync(path.join(__dirname, footerUrl), 'utf8');

        this.mail = this.mail.replace('#FOOTER', footer);
    }

    appendReplacement(keyword, value){
        this.replacedWords.set(keyword, value);
    }

    updateContent(){

        //  THIS METHOD YOU BE SURCHARGE TO CUSTOMIZE MAIL

        let tmpMail = this.mail ;
        let  replacer ;
        this.replacedWords.forEach(function(value, key) {
            replacer = new RegExp(key, 'g')
            tmpMail = tmpMail.replace(replacer , value) ;
           
          });
         
          this.mail = tmpMail;
        
    }     
    
    getContent(){

        this.readBody(this.bodyUrl) ;
        this.setFooter();
        this.updateContent();
        
    }

    async  send(){

        this.getContent() ;
       
        if ( !processing.isNullOrEmpty(this.toEmail)) {
        
            await mail.nodemailer_mailgun_sender({
                "from": `${constants.business.APP_NAME} <${constants.business.EMAIL_NOREPLY}>`,
                "to": this.toEmail,
                "subject":this.subject,
                "html": this.mail
            });
        }
       

    }

  }


  class RegistrationMail extends MailTemplate{

    constructor(name,toEmail, activationLink,from = constants.business.EMAIL_NOREPLY) {
        
        super();

        this.subject = `Confirmation inscription ${constants.business.APP_NAME}`;

        this.toEmail = toEmail ;

        this.bodyUrl = REGISTRATION_TEMPLATE  ;

         this.appendReplacement(constants.technical.TEMPLATE_MARKER_NAME, name) ;

         this.appendReplacement(constants.technical.TEMPLATE_MARKER_LINK_ACCOUNT_ACTIVATION, activationLink) ;
    }


    appendReplacement(keyword, value){

        this.replacedWords.set(keyword, value);
    }


  }
  

  class AlerteMail extends MailTemplate{

    constructor(name,toEmail, alerteMessage , from = constants.business.EMAIL_NOREPLY) {
        
        super();

        this.subject = `Alertes sur ${constants.business.APP_NAME}` ;

        this.toEmail = toEmail ;

        this.bodyUrl = ALERTE_TEMPLATE  ;

         this.appendReplacement(constants.technical.TEMPLATE_MARKER_NAME, name) ;

         this.appendReplacement(constants.technical.TEMPLATE_MARKER_MESSAGE, alerteMessage) ;
    }


    appendReplacement(keyword, value){

        this.replacedWords.set(keyword, value);
    }


  }


  module.exports = {
    RegistrationMail:RegistrationMail,
    AlerteMail:AlerteMail
};
