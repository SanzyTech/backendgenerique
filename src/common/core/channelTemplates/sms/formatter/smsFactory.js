const fs = require('fs');
const path = require('path');
const SMS_TEMPLATE = require('../template/fr/smsTemplate');
const FOOTER_TEMPLATE = '../template/fr/footer.html';
const constants = require('../../../constants/constants');
const processing = require('../../../processing/processing');
const smsService = require('../../../../api/services/channels/sms');


  class SmsTemplate {
    constructor(name, to) {

      this.name = name ;
      this.to = to ;
      this.body = '';
      this.replacedWords = new Map();

    }


    readBody(){

        // WE REPLACE APP_NAME AND SO ON
        this.appendReplacement(constants.technical.TEMPLATE_MARKER_APP_NAME, constants.business.APP_NAME) ;
    }

    appendReplacement(keyword, value){
        this.replacedWords.set(keyword, value);
    }

    updateContent(){

        //  THIS METHOD YOU BE SURCHARGE TO CUSTOMIZE MAIL
        let tmpSms = this.body ;
        let  replacer ;
        this.replacedWords.forEach(function(value, key) {
            replacer = new RegExp(key, 'g')
            tmpSms = tmpSms.replace(replacer , value) ;
           
          });
         
        this.body = tmpSms;
        
    }     
    
    getContent(){

        this.readBody() ;
        this.updateContent();
            
    }

    async  send(){

        this.getContent() ;
        if ( !processing.isNullOrEmpty(this.to)) {
        
           /* await smsService.send_notification( {
                "text": this.body
                , "to":this.to
            });*/

        }

    }

  }


  class RegistrationSms extends SmsTemplate{

    constructor(name, to , activationLink) {
        
        super(name ,to);

        this.body = SMS_TEMPLATE.registrationSms;  

         this.appendReplacement(constants.technical.TEMPLATE_MARKER_NAME, name) ;
         this.appendReplacement(constants.technical.TEMPLATE_MARKER_LINK_ACCOUNT_ACTIVATION, activationLink) ;
    }


    appendReplacement(keyword, value){

        this.replacedWords.set(keyword, value);
    }


  }
  

  module.exports = {
    RegistrationSms:RegistrationSms
};
