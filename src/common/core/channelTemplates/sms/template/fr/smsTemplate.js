
// CONFIG
const config = require('../../../../../../../config/environnement/config');

//CONSTANT
const constant = require('../../../../constants/constants');
const registrationSms = ` Bienvenue sur ${constant.business.APP_NAME} #USER_NAME,\nAfin de valider votre compte, nous vous prions de cliquer sur le lien suivant:\n #LINK_ACCOUNT_ACTIVATION\nEn cas de problème, vous pouvez nous contacter aux numéros suivants:  ${config.phonenumbers}. A très bientôt.`


module.exports = {
    registrationSms
  }