const business = require("./business")
const technical = require("./technical")

module.exports = {
    business,
    technical
  }