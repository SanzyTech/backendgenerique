
const API = "api";
const VERSION_1 = "v1";
// TO DO ADD different layer
const LAYER_MODEL = "model" ;
const LAYER_SERVICE = "service" ;
const LAYER_CONTROLLER = "controller" ;
const LAYER_ROUTE= "routes" ;
const LAYER_VALIDATOR = "validator" ;
const INDEX = "index";
const PARTICULAR = "particular";
const USER = "users";
const AFFILIATION = "affiliation";
const COMPANY = "company";
const ACCOUNT = "account";
const PROSPECT= "prospect";
// USER TYPE 
const CLIENT = "client";
const ADMINISTRATOR = "administrator" ;
//
const AUTHENTIFICATION = "authentification"
//
const REGISTRATION ="registration" ;

// IT
const IT_LEVEL= "it" ;
const MARKETING_LEVEL = "marketing" ;
const CUSTOMERS_SERVICE_LEVEL = "costumers_service" ;

// STATE 

const STATE_CANCELLED="cancelled"
// TEMPLATE

const TEMPLATE_MARKER_APP_NAME = '#APP_NAME' ;
const TEMPLATE_MARKER_URL_LOGO = '#URL_LOGO' ;

const TEMPLATE_MARKER_NAME = '#USER_NAME' ;
const TEMPLATE_MARKER_LINK_ACCOUNT_ACTIVATION = '#LINK_ACCOUNT_ACTIVATION' ;
const TEMPLATE_MARKER_MESSAGE = '#MESSAGE' ;

module.exports = {
    API,
    VERSION_1,
    LAYER_MODEL,
    LAYER_SERVICE,
    LAYER_CONTROLLER,
    LAYER_VALIDATOR,
    LAYER_ROUTE,
    INDEX,
    USER,
    ACCOUNT,
    COMPANY,
    CLIENT,
    AUTHENTIFICATION,
    ADMINISTRATOR,
    AFFILIATION,
    PARTICULAR,
    PROSPECT,
    REGISTRATION,
    STATE_CANCELLED,
    IT_LEVEL,
    MARKETING_LEVEL,
    CUSTOMERS_SERVICE_LEVEL,
    TEMPLATE_MARKER_NAME,
    TEMPLATE_MARKER_APP_NAME,
    TEMPLATE_MARKER_URL_LOGO,
    TEMPLATE_MARKER_LINK_ACCOUNT_ACTIVATION,
    TEMPLATE_MARKER_MESSAGE
  }