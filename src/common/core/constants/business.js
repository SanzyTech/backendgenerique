 const APP_NAME="Jobaas-CV" ;
 const BUSINESS_DESCRIPTION= `${APP_NAME}  est le leader dans l'accompagnement professionnel dans lélaboration des CV et des lettres de motivation` ;
 const URL_LOGO= `https://nodejstemplateresource.s3.eu-west-3.amazonaws.com/images/logoTemplate.jpg?response-content-disposition=inline&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEJb%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMyJHMEUCICjcFn9Y1cLJIhmeFvZycwL5c2eH%2F7uDlLA8uSWexbZhAiEA2X1HazvcJVxxKqNQ5tsXuSOdPJUKRCDaLD4gTT5Mzqsq5AIITxABGgwxOTY4Nzk4OTU5OTkiDDYUEt7xLknsOzHIBirBAtq2c8fi0Lw3yK7gCL33KD0JYN2SiVgR82THgRyDnceNTEovo0BNhFVob42E%2FyQ8xUu7JT2r3D2CYMESSJbs0RuzfoEJkCggpn%2FHCVk7a%2FjeDTBcOfK2lA%2FebLLJ8sy0JCguz9X%2B8cH3xMtBmgAU8sC2vJ4N5cUFc9abgTEJ%2BmOVWodAwImmXCIwwEhv4Gk0lf4raEGSKmxZYlFqq1XC0GDdc%2BI0orv2cm105pKBnv3LFv4UBye4HOZlprQv%2F%2FQIklIQZznzHjZAeoq%2Fs7ea3UAmer7FYNcE0pm%2FXGjXamjAP5ANmh%2FVMn%2Fvch4AwoOW1OZslHdzrRj0hTHw%2FosZk59BH6TX2VwcJgGJyT24T%2BHVnTh2Nx9uxIwny8sSsQy3FSID7B%2BWWVyKk0xQrwNiGXpTDRugd6UaIXGJTyCPvHFpIDCpzIyTBjqzAoWyYM98ZTkD1JtAlzx0qW3L7uBv4oubnUwSR2kzAeRqhqgok0CQOMC9SCx5UCPubhfwTxm3YAzR2ifSTJAlQxm3MOlQPk1q4S9H9V2D%2BWx1wOMyNNHG4vqO2PtdY8f6vKYk4Q9miith9GiHpHT3oBX3kI4N%2BFU6nuR6sHuQiTH3SaN0KFOKb1%2F1azUGuNlBZQUUQyUDBG22nbYBG96cD5cgFegIJsHdlurQVAFL%2B6kwkPdFdCdeO2bdbW3k00etuRX2qElCGsRAIelJXIdcE6TucBrmjGpvqprLYdYn2%2FAUgY8uxELoMaMe7b4mIix60gDSNhRi5cbvE%2BIj7LZEh%2Bk2euAHicGo3l0krRrDcnkBZkGQITfweB%2FeGmkYyDPqEgrFcCkdBDbaMqM9bFyizZXDb5k%3D&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20220422T221217Z&X-Amz-SignedHeaders=host&X-Amz-Expires=299&X-Amz-Credential=ASIAS3VXUZW7Y4JHH7FZ%2F20220422%2Feu-west-3%2Fs3%2Faws4_request&X-Amz-Signature=6fb296797a2be32f11171d55b665e788fdc76998f8bdc32023cff4095c21d6ae` ;


 // EMAIL

 const DOMAIN_NAME ="jobaas.cm"
 const EMAIL_NOREPLY = "no_reply@jobaas.cm" ;
 module.exports = {
    APP_NAME,
    BUSINESS_DESCRIPTION,
    URL_LOGO,
    EMAIL_NOREPLY
  }