



 const isNullOrEmpty = (value) => {

    if(typeof value !== 'undefined' && value && value.length !=0 ){
        return false 
    } else{
        return true; 
    } 
};

 const getLanguage = (language) => {

    return  language ? language : 'en';;
};
const dotNotate = (obj, target, prefix) => {
    target = target || {};
    prefix = prefix || "";

    Object.keys(obj).forEach(function (key) {
        if (obj[key] !== null && typeof (obj[key]) === "object") {
            dotNotate(obj[key], target, prefix + key + ".");
        } else {
            return target[prefix + key] = obj[key];
        }
    });
    return target;
};

const updateMeanRatings = (oldMean, newNote, old_number_eval) => {
    return (oldMean * old_number_eval + newNote) / (old_number_eval + 1)
};

const conditionsPassword = (pass) => {
    let valid = true;
    let score = 0;
    if (!pass)
        return {valid: false, score: 0};

    //The password should have at least 8 characters
    if (pass.length < 8) {
        return {valid: false, score: score};
    } else {
        // award every unique letter until 5 repetitions
        score = 30;
        let letters = {};
        for (let i = 0; i < pass.length; i++) {
            letters[pass[i]] = (letters[pass[i]] || 0) + 1;
            score += 5.0 / letters[pass[i]];
        }

        // points for mixing it up
        let variations = {
            digits: /\d/.test(pass),
            lower: /[a-z]/.test(pass),
            upper: /[A-Z]/.test(pass)
        };

        let variationCount = 0;
        for (let check in variations) {
            if (variations[check] === true) {
                variationCount += 1;
            } else {
                valid = false;
            }

        }
        score += (variationCount - 1) * 10;

        return {
            valid: valid,
            score: score
        };
    }
};

const makePassword = () => {
    const collectionOfLetters = "*@.+/#ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let generatedPassword = "";
    const size = collectionOfLetters.length;
    for (let i = 0; i <= 8; ++i) {
        generatedPassword = generatedPassword + collectionOfLetters.charAt(Math.floor(Math.random() * size));
    }
    return generatedPassword + Math.floor(Math.random() * 9);
};

const getAge = (dateString) => {
    try {
        const today = new Date();
        const birthDate = new Date(dateString);
        return today.getFullYear() - birthDate.getFullYear();
    } catch (e) {
        throw new Error('It is not a date');
    }
};

const computePrice = (jobPayment, nbEmployees = 1, typeMission = 'punctual') => {
    //To upgrade
    if (typeMission === 'permanent') {
        let price = 0;
        for (let i = 0; i < nbEmployees; i++) {
            if (i === 0) {
                price = price + jobPayment * 0.2;
            } else if (i === 1) {
                price = price + jobPayment * 0.18;
            } else if (i === 2) {
                price = price + jobPayment * 0.15;
            } else if (i > 2) {
                price = price + jobPayment * 0.12;
            }
        }
        return price;
    } else {
        if (jobPayment >= 0 && jobPayment < 5000) {
            return jobPayment * 0.8;
        } else if (jobPayment >= 5000 && jobPayment < 10000) {
            return jobPayment * 0.85;
        } else if (jobPayment >= 10000 && jobPayment < 50000) {
            return jobPayment * 0.88;
        } else if (jobPayment >= 50000) {
            return jobPayment * 0.90;
        }
    }
};

const incrementDate = (currentDate, nbDaysToAdd) => {
    //current Date must be instance of new Date()
    currentDate.setDate(currentDate.getDate() + nbDaysToAdd);
    return currentDate.toISOString().slice(0, 10);
};


const formatFilterParams = (filterParams,excludedKeys) => {
    for (let [key, value] of Object.entries(filterParams)) {
        if (typeof value == "string" && value.length !== 0 && !excludedKeys.includes(key)) {
            if ((new Date(value)).toString() === "Invalid Date") {
                filterParams[key] = new RegExp(value + ".*", "i");
            }
        } else {
            if (value !== undefined && value !== "") {
                filterParams[key] = value
            } else {
                delete filterParams[key];
            }
        }
    }
    return  filterParams;
};

const toCapitalize = (text) => {
    let textLowed = text.toLowerCase();
    return textLowed.charAt(0).toUpperCase() + textLowed.slice(1);
};

const removeSpecialCharacter = (text) => {
    //TODO NE PAS TENIR COMPTE DES TIRETS DE 6
    return text.replace(/[&\/\\#^+()$~%.'":*?<>{}!@]/g, '');
};


module.exports = {
    isNullOrEmpty:isNullOrEmpty,
    getLanguage:getLanguage,
    dotNotate: dotNotate,
    makePassword: makePassword,
    updateMeanRatings: updateMeanRatings,
    securePassword: conditionsPassword,
    getAge: getAge,
    computePrice: computePrice,
    toCapitalize: toCapitalize,
    formatFilterParams:formatFilterParams,
    incrementDate: incrementDate,
    removeSpecialCharacter: removeSpecialCharacter
};
