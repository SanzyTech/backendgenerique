//LIBRARIES

const fs = require('fs');
const path = require('path');
const {validationResult} = require('express-validator');

const errorProcessing = require('../../../../common/core/error/error');
const bcrypt = require('bcrypt');
const processing = require('../../../../common/core/processing/processing');

// SERVICES 


const offerService = require('../../services/offers');
const roleService = require('../../../../common/api/services/accounts/role');
const email_sender = require('../../../../common/api/services/channels/email');
const smsService = require('../../../../common/api/services/channels/sms');
const ADMIN_RIGHTS = require('../../../../common/core/types/enum').adminRights;
const smsFunctions = require('../../../../common/api/rest/controllers/sms');
const notificationService = require('../../../../common/api/services/channels/notification');

//CONFIGS 

const config = require('../../../../../config/environnement/config');
const no_mail = config.no_mail;
const EMPLOYER = config.permissionLevels.EMPLOYER_USER;
const EMPLOYEE = config.permissionLevels.EMPLOYEE_USER;
const saltRounds = config.sr;

// LOGGERS 

const loggerGenerator = require("../../../../common/core/logger/logger")
const constants = require("../../../../common/core/constants/constants")
const layerName = constants.technical.LAYER_CONTROLLER
const controllerName = "offers"
const logger = loggerGenerator.getLoger(layerName, controllerName)


 class Offer {

    constructor(){
    }

    async getAllOffers(req, res) {

        logger.info("Call getAllOffers");

        let lang;

        try {
            const limit = req.query.limit && req.query.limit <= 100 ? parseInt(req.query.limit) : 10;
            let preview = req.query.preview ? req.query.preview : false;
            let level = req.query.level  ? parseInt(req.query.level) : 1;
            let page = 0;
            if (req.query) {
                if (req.query.page) {
                    req.query.page = parseInt(req.query.page);
                    page = Number.isInteger(req.query.page) ? req.query.page : 0;
                }
            }
            let Offers;
            let tmp = [];
            let result;

            result = await offerService.getAllOffers(limit, page, req.query, level, preview);
            
            return res.status(200).json({
                'data': result
            });
        } catch (e) {
            logger.info(e.message);
            const err = new errorProcessing.ServerError(e, lang);
            return res.status(500).json({
                'message': errorProcessing.process(err)
            });
        }
    }

async createOffer(req, res)  {

    logger.info("Call createOffer");

    let message;

    let lang = req.query.lang ? req.query.lang : 'en';

    try {
        //delete field with default value
        
        const offer = await offerService.createOffer(req.body);
        message = lang === 'fr' ? 'Nouveau offer créé ' : 'new offer was created';
        logger.info('new offer was created : ' + offer.id);
        return res.status(200).json({
            'message': message,
            'data': offer,
        });
    } catch (e) {

        logger.info(e.message);
        const err = new errorProcessing.ServerError(e, lang);
        const message = await err.process(false);
        console.log("message "+message);
        return res.status(500).json({
            'message': message
        });
    }
}

 async getOfferById(req, res)  {

    logger.info("Call getOfferById") ;

    let lang = req.query.lang ? req.query.lang : 'en';
    let message;

    try {
        let offer = await offerService.getOfferById(req.params.idOffer);
        if (!offer) {
            message = errorProcessing.generateResponse(controllerName, "get", 404, req.query.lang);
            logger.info('offer not found');
            return res.status(404).json({
                'message': message
            });
        }
   
        logger.info('the offer found by id : ' + req.params.idOffer);
        return res.status(200).json({
            'data': {"offer": offer}
        });
    } catch (e) {
        logger.info(e.message);
        const err = new errorProcessing.ServerError(e, lang);
        return res.status(500).json({
            'message': errorProcessing.process(err)
        });
    }
};

async updateOffer(req, res)  {

    logger.info("Call updateOffer");

    let lang = req.query.lang ? req.query.lang : 'en';
  
    delete req.body.state;

    try {

        const offer = await offerService.updateOffer(req.params.idOffer, req.body);
        let message;
        if (!offer) {
            message = errorProcessing.generateResponse(controllerName,"update", 404, req.query.lang);
            return res.status(404).json({
                'message': message
            });
        }
        message = errorProcessing.generateResponse(controllerName,"update", 404, req.query.lang);
        logger.info('the Offer was updated by id : ' + req.params.idOffer);
        return res.status(200).json({
            'message':message,
            'data': offer,
        });
    } catch (e) {

        logger.info(e.message);
        const err = new errorProcessing.ServerError(e, lang);
        return res.status(500).json({
            'message': errorProcessing.process(err)
        });
    }
};

async cancelOffer (req, res){

    logger.info("Call cancelOffer");

    let message;
    let lang = req.query.lang ? req.query.lang : 'en';

    try {
        const offerBeforeDelete = await offerService.getOffer(req.params.idOffer);
        // if the offer is already deleted return an error
        message = lang === "fr" ? "ce offer  a déjà été annulé" : "this offer has already been cancelled";
        // check the current offer's state
        if (offerBeforeDelete.state === "cancelled") {
            return res.status(409).json({
                'message': message
            });
        }

        const offer = await offerService.cancelOffer(req.params.idOffer);
        if (offer) {
            //TODO   Notifiy

            // We update stats
            message = req.query.lang === "fr" ? "Suppression du offer " : "the offer was deleted";
            return res.status(200).json({
                'message': message
            });
        } else {
            message = errorProcessing.generateResponse(controllerName,"update", 404, req.query.lang); 
            return res.status(404).json({
                'message': message
            });
        }
    } catch (e) {
        logger.info(e.message);
        const err = new errorProcessing.ServerError(e, lang);
        return res.status(500).json({
            'message': errorProcessing.process(err)
        });
    }
};

async  deleteOffer(req, res) {

    logger.info("Call deleteOffer");

    let message;
    let lang = req.query.lang ? req.query.lang : 'en';

    try {
        // TO DO ADD A VALIDATOR THAT CHECK IF EXISTS

        const isOfferDeleted = await offerService.deleteOffer(req.params.idOffer);
        // if the offer is already deleted return an error
        message = req.query.lang === "fr" ? "le offer ayant pour id : " + req.params.idOffer + " a déjà été supprimé " : "the offer identified by id : " + req.params.idOffer + " has already been deleted";
        // check the current offer's state
        if (isOfferDeleted === false) {
            return res.status(409).json({
                'message': message
            });
        } else {
            message = req.query.lang === "fr" ? "le offer ayant pour id : " + req.params.idOffer + " a  été supprimé " : message = 'the offer identified by id : ' + req.params.idOffer + " has  been deleted";
            return res.status(200).json({
                'message': message
            });
        }
    } catch (e) {
        logger.info(e.message);
        const err = new errorProcessing.ServerError(e, lang);
        return res.status(500).json({
            'message': errorProcessing.process(err)
        });
    }
}




}


module.exports = new Offer()