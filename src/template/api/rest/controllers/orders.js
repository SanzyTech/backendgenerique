//LIBRARIES

const fs = require('fs');
const path = require('path');
const {validationResult} = require('express-validator');

const errorProcessing = require('../../../../common/core/error/error');
const bcrypt = require('bcrypt');
const processing = require('../../../../common/core/processing/processing');

// SERVICES 


const orderService = require('../../services/orders');
const roleService = require('../../../../common/api/services/accounts/role');
const email_sender = require('../../../../common/api/services/channels/email');
const smsService = require('../../../../common/api/services/channels/sms');
const ADMIN_RIGHTS = require('../../../../common/core/types/enum').adminRights;
const smsFunctions = require('../../../../common/api/rest/controllers/sms');
const notificationService = require('../../../../common/api/services/channels/notification');

//CONFIGS 

const config = require('../../../../../config/environnement/config');
const no_mail = config.no_mail;
const EMPLOYER = config.permissionLevels.EMPLOYER_USER;
const EMPLOYEE = config.permissionLevels.EMPLOYEE_USER;
const saltRounds = config.sr;

// LOGGERS 

const loggerGenerator = require("../../../../common/core/logger/logger")
const constants = require("../../../../common/core/constants/constants")
const layerName = constants.technical.LAYER_CONTROLLER
const controllerName = "orders"
const logger = loggerGenerator.getLoger(layerName, controllerName)


 class Order {

    constructor(){
    }

    async getAllOrders(req, res) {

        logger.info("Call getAllOrders");

        let lang;

        try {
            const limit = req.query.limit && req.query.limit <= 100 ? parseInt(req.query.limit) : 10;
            let preview = req.query.preview ? req.query.preview : false;
            let level = req.query.level  ? parseInt(req.query.level) : 1;
            let page = 0;
            if (req.query) {
                if (req.query.page) {
                    req.query.page = parseInt(req.query.page);
                    page = Number.isInteger(req.query.page) ? req.query.page : 0;
                }
            }
            let Orders;
            let tmp = [];
            let result;

            result = await orderService.getAllOrders(limit, page, req.query, level, preview);
            
            return res.status(200).json({
                'data': result
            });
        } catch (e) {
            logger.info(e.message);
            const err = new errorProcessing.ServerError(e, lang);
            return res.status(500).json({
                'message': errorProcessing.process(err)
            });
        }
    }

async createOrder(req, res)  {

    logger.info("Call createOrder");

    let message;

    let lang = req.query.lang ? req.query.lang : 'en';

    try {
        //delete field with default value
        
        const order = await orderService.createOrder(req.body);
        message = lang === 'fr' ? 'Nouveau order créé ' : 'new order was created';
        logger.info('new order was created : ' + order.id);
        return res.status(200).json({
            'message': message,
            'data': order,
        });
    } catch (e) {

        logger.info(e.message);
        const err = new errorProcessing.ServerError(e, lang);
        const message = await err.process(false) ;
        return res.status(500).json({
            'message': err.process(message)
        });
    }
}

 async getOrderById(req, res)  {

    logger.info("Call getOrderById") ;

    let lang = req.query.lang ? req.query.lang : 'en';
    let message;

    try {
        let order = await orderService.getOrderById(req.params.idOrder);
        if (!order) {
            message = errorProcessing.generateResponse(controllerName, "get", 404, req.query.lang);
            logger.info('order not found');
            return res.status(404).json({
                'message': message
            });
        }
   
        logger.info('the order found by id : ' + req.params.idOrder);
        return res.status(200).json({
            'data': {"order": order}
        });
    } catch (e) {
        logger.info(e.message);
        const err = new errorProcessing.ServerError(e, lang);
        return res.status(500).json({
            'message': errorProcessing.process(err)
        });
    }
};

async updateOrder(req, res)  {

    logger.info("Call updateOrder");

    let lang = req.query.lang ? req.query.lang : 'en';
  
    delete req.body.state;

    try {

        const order = await orderService.updateOrder(req.params.idOrder, req.body);
        let message;
        if (!order) {
            message = errorProcessing.generateResponse(controllerName,"update", 404, req.query.lang);
            return res.status(404).json({
                'message': message
            });
        }
        message = errorProcessing.generateResponse(controllerName,"update", 404, req.query.lang);
        logger.info('the Order was updated by id : ' + req.params.idOrder);
        return res.status(200).json({
            'message':message,
            'data': order,
        });
    } catch (e) {

        logger.info(e.message);
        const err = new errorProcessing.ServerError(e, lang);
        return res.status(500).json({
            'message': errorProcessing.process(err)
        });
    }
};

async cancelOrder (req, res){

    logger.info("Call cancelOrder");

    let message;
    let lang = req.query.lang ? req.query.lang : 'en';

    try {
        const orderBeforeDelete = await orderService.getOrder(req.params.idOrder);
        // if the order is already deleted return an error
        message = lang === "fr" ? "ce order  a déjà été annulé" : "this order has already been cancelled";
        // check the current order's state
        if (orderBeforeDelete.state === "cancelled") {
            return res.status(409).json({
                'message': message
            });
        }

        const order = await orderService.cancelOrder(req.params.idOrder);
        if (order) {
            //TODO   Notifiy

            // We update stats
            message = req.query.lang === "fr" ? "Suppression du order " : "the order was deleted";
            return res.status(200).json({
                'message': message
            });
        } else {
            message = errorProcessing.generateResponse(controllerName,"update", 404, req.query.lang); 
            return res.status(404).json({
                'message': message
            });
        }
    } catch (e) {
        logger.info(e.message);
        const err = new errorProcessing.ServerError(e, lang);
        return res.status(500).json({
            'message': errorProcessing.process(err)
        });
    }
};

async  deleteOrder(req, res) {

    logger.info("Call deleteOrder");

    let message;
    let lang = req.query.lang ? req.query.lang : 'en';

    try {
        // TO DO ADD A VALIDATOR THAT CHECK IF EXISTS

        const isOrderDeleted = await orderService.deleteOrder(req.params.idOrder);
        // if the order is already deleted return an error
        message = req.query.lang === "fr" ? "le order ayant pour id : " + req.params.idOrder + " a déjà été supprimé " : "the order identified by id : " + req.params.idOrder + " has already been deleted";
        // check the current order's state
        if (isOrderDeleted === false) {
            return res.status(409).json({
                'message': message
            });
        } else {
            message = req.query.lang === "fr" ? "le order ayant pour id : " + req.params.idOrder + " a  été supprimé " : message = 'the order identified by id : ' + req.params.idOrder + " has  been deleted";
            return res.status(200).json({
                'message': message
            });
        }
    } catch (e) {
        logger.info(e.message);
        const err = new errorProcessing.ServerError(e, lang);
        return res.status(500).json({
            'message': errorProcessing.process(err)
        });
    }
}




}


module.exports = new Order()