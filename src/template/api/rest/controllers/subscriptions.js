//LIBRARIES

const fs = require('fs');
const path = require('path');
const {validationResult} = require('express-validator');

const errorProcessing = require('../../../../common/core/error/error');
const bcrypt = require('bcrypt');
const processing = require('../../../../common/core/processing/processing');

// SERVICES 


const subscriptionService = require('../../services/subscriptions');
const roleService = require('../../../../common/api/services/accounts/role');
const email_sender = require('../../../../common/api/services/channels/email');
const smsService = require('../../../../common/api/services/channels/sms');
const ADMIN_RIGHTS = require('../../../../common/core/types/enum').adminRights;
const smsFunctions = require('../../../../common/api/rest/controllers/sms');
const notificationService = require('../../../../common/api/services/channels/notification');

//CONFIGS 

const config = require('../../../../../config/environnement/config');
const no_mail = config.no_mail;
const EMPLOYER = config.permissionLevels.EMPLOYER_USER;
const EMPLOYEE = config.permissionLevels.EMPLOYEE_USER;
const saltRounds = config.sr;

// LOGGERS 

const loggerGenerator = require("../../../../common/core/logger/logger")
const constants = require("../../../../common/core/constants/constants")
const layerName = constants.technical.LAYER_CONTROLLER
const controllerName = "subscriptions"
const logger = loggerGenerator.getLoger(layerName, controllerName)


 class Subscription {

    constructor(){
    }

    async getAllSubscriptions(req, res) {

        logger.info("Call getAllSubscriptions");

        let lang;

        try {
            const limit = req.query.limit && req.query.limit <= 100 ? parseInt(req.query.limit) : 10;
            let preview = req.query.preview ? req.query.preview : false;
            let level = req.query.level  ? parseInt(req.query.level) : 1;
            let page = 0;
            if (req.query) {
                if (req.query.page) {
                    req.query.page = parseInt(req.query.page);
                    page = Number.isInteger(req.query.page) ? req.query.page : 0;
                }
            }
            let Subscriptions;
            let tmp = [];
            let result;

            result = await subscriptionService.getAllSubscriptions(limit, page, req.query, level, preview);
            
            return res.status(200).json({
                'data': result
            });
        } catch (e) {
            logger.info(e.message);
            const err = new errorProcessing.ServerError(e, lang);
            return res.status(500).json({
                'message': errorProcessing.process(err)
            });
        }
    }

async createSubscription(req, res)  {

    logger.info("Call createSubscription");

    let message;

    let lang = req.query.lang ? req.query.lang : 'en';

    try {
        //delete field with default value
        
        const subscription = await subscriptionService.createSubscription(req.body);
        message = lang === 'fr' ? 'Nouveau subscription créé ' : 'new subscription was created';
        logger.info('new subscription was created : ' + subscription.id);
        return res.status(200).json({
            'message': message,
            'data': subscription,
        });
    } catch (e) {

        logger.info(e.message);
        const err = new errorProcessing.ServerError(e, lang);
        const message = await err.process(false) ;
        return res.status(500).json({
            'message': err.process(message)
        });
    }
}

 async getSubscriptionById(req, res)  {

    logger.info("Call getSubscriptionById") ;

    let lang = req.query.lang ? req.query.lang : 'en';
    let message;

    try {
        let subscription = await subscriptionService.getSubscriptionById(req.params.idSubscription);
        if (!subscription) {
            message = errorProcessing.generateResponse(controllerName, "get", 404, req.query.lang);
            logger.info('subscription not found');
            return res.status(404).json({
                'message': message
            });
        }
   
        logger.info('the subscription found by id : ' + req.params.idSubscription);
        return res.status(200).json({
            'data': {"subscription": subscription}
        });
    } catch (e) {
        logger.info(e.message);
        const err = new errorProcessing.ServerError(e, lang);
        return res.status(500).json({
            'message': errorProcessing.process(err)
        });
    }
};

async updateSubscription(req, res)  {

    logger.info("Call updateSubscription");

    let lang = req.query.lang ? req.query.lang : 'en';
  
    delete req.body.state;

    try {

        const subscription = await subscriptionService.updateSubscription(req.params.idSubscription, req.body);
        let message;
        if (!subscription) {
            message = errorProcessing.generateResponse(controllerName,"update", 404, req.query.lang);
            return res.status(404).json({
                'message': message
            });
        }
        message = errorProcessing.generateResponse(controllerName,"update", 404, req.query.lang);
        logger.info('the Subscription was updated by id : ' + req.params.idSubscription);
        return res.status(200).json({
            'message':message,
            'data': subscription,
        });
    } catch (e) {

        logger.info(e.message);
        const err = new errorProcessing.ServerError(e, lang);
        return res.status(500).json({
            'message': errorProcessing.process(err)
        });
    }
};

async cancelSubscription (req, res){

    logger.info("Call cancelSubscription");

    let message;
    let lang = req.query.lang ? req.query.lang : 'en';

    try {
        const subscriptionBeforeDelete = await subscriptionService.getSubscription(req.params.idSubscription);
        // if the subscription is already deleted return an error
        message = lang === "fr" ? "ce subscription  a déjà été annulé" : "this subscription has already been cancelled";
        // check the current subscription's state
        if (subscriptionBeforeDelete.state === "cancelled") {
            return res.status(409).json({
                'message': message
            });
        }

        const subscription = await subscriptionService.cancelSubscription(req.params.idSubscription);
        if (subscription) {
            //TODO   Notifiy

            // We update stats
            message = req.query.lang === "fr" ? "Suppression du subscription " : "the subscription was deleted";
            return res.status(200).json({
                'message': message
            });
        } else {
            message = errorProcessing.generateResponse(controllerName,"update", 404, req.query.lang); 
            return res.status(404).json({
                'message': message
            });
        }
    } catch (e) {
        logger.info(e.message);
        const err = new errorProcessing.ServerError(e, lang);
        return res.status(500).json({
            'message': errorProcessing.process(err)
        });
    }
};

async  deleteSubscription(req, res) {

    logger.info("Call deleteSubscription");

    let message;
    let lang = req.query.lang ? req.query.lang : 'en';

    try {
        // TO DO ADD A VALIDATOR THAT CHECK IF EXISTS

        const isSubscriptionDeleted = await subscriptionService.deleteSubscription(req.params.idSubscription);
        // if the subscription is already deleted return an error
        message = req.query.lang === "fr" ? "le subscription ayant pour id : " + req.params.idSubscription + " a déjà été supprimé " : "the subscription identified by id : " + req.params.idSubscription + " has already been deleted";
        // check the current subscription's state
        if (isSubscriptionDeleted === false) {
            return res.status(409).json({
                'message': message
            });
        } else {
            message = req.query.lang === "fr" ? "le subscription ayant pour id : " + req.params.idSubscription + " a  été supprimé " : message = 'the subscription identified by id : ' + req.params.idSubscription + " has  been deleted";
            return res.status(200).json({
                'message': message
            });
        }
    } catch (e) {
        logger.info(e.message);
        const err = new errorProcessing.ServerError(e, lang);
        return res.status(500).json({
            'message': errorProcessing.process(err)
        });
    }
}




}


module.exports = new Subscription()