const express = require('express');

const subscriptionController = require('../controllers/subscriptions');

const validationSecurity = require('../../../../common/api/rest/controllers/security/validation');
const permissionSecurity = require('../../../../common/api/rest/controllers/security/permission');
const sanitizeSecurity = require('../../../../common/api/rest/controllers/security/sanitize');

const router = express.Router();
const config = require('../../../../../config/environnement/config');

const ADMIN = config.permissionLevels.ADMIN;
const EMPLOYER = config.permissionLevels.EMPLOYER_Subscription;
const EMPLOYEE = config.permissionLevels.EMPLOYEE_Subscription;
const CONTROLLER = config.permissionLevels.CONTROLLER_Subscription;
const SUP_ADMIN = config.permissionLevels.SUP_ADMIN;
const ENTREPRISE = config.permissionLevels.ENTREPRISE_Subscription;

const busboy = require('connect-busboy');
const busboyBodyParser = require('busboy-body-parser');

router.get('/:idSubscription',
    [
        //validationSecurity.validJWTNeeded,
        //permissionSecurity.permissionLevelRequired([ADMIN, CONTROLLER, SUP_ADMIN]),
        subscriptionController.getSubscriptionById,
    ]);




    router.post('/',
    [
        //  TO DO implement validation field 
        //sanitizeSecurity.validate('createSubscription'),
        subscriptionController.createSubscription
    ]);




router.put('/:idSubscription',
    [
       // validationSecurity.validJWTNeeded,
       // permissionSecurity.permissionLevelRequired([ADMIN, CONTROLLER, SUP_ADMIN]),
        subscriptionController.updateSubscription
    ]);



router.get('/',
    [
       // validationSecurity.validJWTNeeded,
       // permissionSecurity.permissionLevelRequired([ADMIN, CONTROLLER, SUP_ADMIN]),
        subscriptionController.getAllSubscriptions
    ]);

router.delete('/:idSubscription',
    [
        //validationSecurity.validJWTNeeded,
        //permissionSecurity.permissionLevelRequired([ADMIN, SUP_ADMIN]),
        subscriptionController.deleteSubscription
    ]);



module.exports = router;
