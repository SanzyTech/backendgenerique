const express = require('express');

const orderController = require('../controllers/orders');

const validationSecurity = require('../../../../common/api/rest/controllers/security/validation');
const permissionSecurity = require('../../../../common/api/rest/controllers/security/permission');
const sanitizeSecurity = require('../../../../common/api/rest/controllers/security/sanitize');

const router = express.Router();
const config = require('../../../../../config/environnement/config');

const ADMIN = config.permissionLevels.ADMIN;
const EMPLOYER = config.permissionLevels.EMPLOYER_Order;
const EMPLOYEE = config.permissionLevels.EMPLOYEE_Order;
const CONTROLLER = config.permissionLevels.CONTROLLER_Order;
const SUP_ADMIN = config.permissionLevels.SUP_ADMIN;
const ENTREPRISE = config.permissionLevels.ENTREPRISE_Order;

const busboy = require('connect-busboy');
const busboyBodyParser = require('busboy-body-parser');

router.get('/:idOrder',
    [
        //validationSecurity.validJWTNeeded,
        //permissionSecurity.permissionLevelRequired([ADMIN, CONTROLLER, SUP_ADMIN]),
        orderController.getOrderById,
    ]);




    router.post('/',
    [
        //  TO DO implement validation field 
        //sanitizeSecurity.validate('createOrder'),
        orderController.createOrder
    ]);




router.put('/:idOrder',
    [
       // validationSecurity.validJWTNeeded,
       // permissionSecurity.permissionLevelRequired([ADMIN, CONTROLLER, SUP_ADMIN]),
        orderController.updateOrder
    ]);



router.get('/',
    [
       // validationSecurity.validJWTNeeded,
       // permissionSecurity.permissionLevelRequired([ADMIN, CONTROLLER, SUP_ADMIN]),
        orderController.getAllOrders
    ]);

router.delete('/:idOrder',
    [
        //validationSecurity.validJWTNeeded,
        //permissionSecurity.permissionLevelRequired([ADMIN, SUP_ADMIN]),
        orderController.deleteOrder
    ]);



module.exports = router;
