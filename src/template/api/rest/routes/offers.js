const express = require('express');

const offerController = require('../controllers/offers');

const validationSecurity = require('../../../../common/api/rest/controllers/security/validation');
const permissionSecurity = require('../../../../common/api/rest/controllers/security/permission');
const sanitizeSecurity = require('../../../../common/api/rest/controllers/security/sanitize');

const router = express.Router();
const config = require('../../../../../config/environnement/config');

const ADMIN = config.permissionLevels.ADMIN;
const EMPLOYER = config.permissionLevels.EMPLOYER_Offer;
const EMPLOYEE = config.permissionLevels.EMPLOYEE_Offer;
const CONTROLLER = config.permissionLevels.CONTROLLER_Offer;
const SUP_ADMIN = config.permissionLevels.SUP_ADMIN;
const ENTREPRISE = config.permissionLevels.ENTREPRISE_Offer;

const busboy = require('connect-busboy');
const busboyBodyParser = require('busboy-body-parser');

router.get('/:idOffer',
    [
        //validationSecurity.validJWTNeeded,
        //permissionSecurity.permissionLevelRequired([ADMIN, CONTROLLER, SUP_ADMIN]),
        offerController.getOfferById,
    ]);




    router.post('/',
    [
        //  TO DO implement validation field 
        //sanitizeSecurity.validate('createOffer'),
        offerController.createOffer
    ]);




router.put('/:idOffer',
    [
       // validationSecurity.validJWTNeeded,
       // permissionSecurity.permissionLevelRequired([ADMIN, CONTROLLER, SUP_ADMIN]),
        offerController.updateOffer
    ]);



router.get('/',
    [
       // validationSecurity.validJWTNeeded,
       // permissionSecurity.permissionLevelRequired([ADMIN, CONTROLLER, SUP_ADMIN]),
        offerController.getAllOffers
    ]);

router.delete('/:idOffer',
    [
        //validationSecurity.validJWTNeeded,
        //permissionSecurity.permissionLevelRequired([ADMIN, SUP_ADMIN]),
        offerController.deleteOffer
    ]);



module.exports = router;
