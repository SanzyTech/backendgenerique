const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const orderSchema = new Schema(
    {
        // name: {
        //     type:String,
        //     required: [true, "name field is required to create a order "]
        // },
        id_subscription: {
            type: String,
            required: [true, "id_subscription field is required to create a subscription "]
        },
        list_of_subscription : {
            type: Array,
            required: [true, "list_of_subscription field is required to create a subscription "]
        },
        total : {
            type: Number,
            required: [true, "total field is required to create a subscription "]
        },
        comment: {
            type: String,
            trim: true
        },

        state: {
            type:String,
            default: "created"
        },
        is_visible: {
            type: Boolean,
            default: true
        },
        creation_date: {
            type: Date,
            default: Date.now()
        }
    },
);

module.exports = mongoose.model('order', orderSchema );
