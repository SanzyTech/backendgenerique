const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const offerSchema = new Schema(
    {
        name: {
            type:String,
            required: [true, "name field is required to create a offer "]
        },
        comment: {
            type: String,
            trim: true
        },
        state: {
            type:String,
            default: "created"
        },
        is_visible: {
            type: Boolean,
            default: true
        },
        creation_date: {
            type: Date,
            default: Date.now()
        }
    },
);

module.exports = mongoose.model('offer', offerSchema );
