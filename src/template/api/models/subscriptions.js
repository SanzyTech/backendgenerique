const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const subscriptionSchema = new Schema(
    {
        // name: {
        //     type:String,
        //     required: [true, "name field is required to create a subscription "]
        // },
         comment: {
            type: String,
            trim: true
        },
        id_offer: {
            type: String,
            required: [true, "id_offer field is required to create a subscription "]
        },
        id_user: {
            type: String,
            required: [true, "id_user field is required to create a subscription "]
        },
        state: {
            type:String,
            default: "created"
        },
        is_visible: {
            type: Boolean,
            default: true
        },
        creation_date: {
            type: Date,
            default: Date.now()
        }
    },
);

module.exports = mongoose.model('subscription', subscriptionSchema );
