//SERVICE

const offerModel = require("../models/offers");

// LOG 
const loggerGenerator = require("../../../common/core/logger/logger")
const constants = require("../../../common/core/constants/constants")
const  layerName = constants.technical.LAYER_SERVICE
const  servicerName = "offers"
const logger = loggerGenerator.getLoger(layerName,servicerName)


class Offer  {


    constructor(){
   
      }

async createOffer (offer)  {

    logger.info("Call createOffer");

    const newoffer = new offerModel(offer);
    let result;
    result = await newoffer.save();
    return result;
};

async  getAllOffers(perPage, page, filterParams, pagination = true)  {

    logger.info("Call getAllOffers ");

    let result;
    // delete the not used filterParams
    delete filterParams.limit;
    delete filterParams.page;
    delete filterParams.lang;

    if (filterParams.tags) {
        filterParams.tags = {"$in": filterParams.tags}
    }
    for (let [key, value] of Object.entries(filterParams)) {
        if (typeof value == "string") {
            if ((new Date(value)).toString() === "Invalid Date") {
                filterParams[key] = new RegExp(value + ".*", "i")
            }
        }
    }
    let length = await offerModel.find(filterParams).count();
    //we  handle pagination
    if (pagination === true) {
        result = await offerModel.find(filterParams).limit(perPage).skip(perPage * page).select('-__v').lean().exec();
    } else {
        result = await offerModel.find(filterParams).select('-__v').lean().exec();
    }
    return {"Offers": result, "length": length};
};

async updateOffer (id, offer, state = 1) {

    logger.info("Call updateOffer "+id);

    let result;
    let currentOffer;

    currentOffer = await offerModel.findById(id).exec();
    
    if (currentOffer) {
        currentOffer.set(offer);
        // console.log(currentoffer);
        result = await currentOffer.save();
        result.toJSON();
        }
    return result;
};


async getOfferById(id) {

    logger.info("Call getOfferById  with id "+id);

    let offer;

    offer = await offerModel.findById(id).select('-__v').lean().exec();
    
    logger.info('get offer service by  id  : '+id);
    return offer;
};

   async getOffer(field, value) {

    logger.info("Call getOffer");

    let offer;
    // offer = field === 'email' ? await offerModel.findOne({'email.value': value}).select('-__v').lean().exec() : await offerModel.findById(value).select('-id -password -__v').lean().exec();
    if (field === 'email') {
        offer = await offerModel.findOne({"email.value": value}).select('-__v').lean().exec();
    } else if (field === 'phoneNumber') {
        offer = await offerModel.findOne({"phoneNumber.value": value}).select('-__v').lean().exec();
    } else if (field === "random_code_for_processes") {
        offer = await offerModel.findOne({"random_code_for_processes.requestId": value}).select('-__v').lean().exec();
    } else{
        offer = await offerModel.findById(value).select('-__v').lean().exec();
    }
    console.log('get offer service by  field : ' + field + ' and value :' + value);
    return offer;
};

async cancelOffer (id)  {

    logger.info("Call cancelOffer"+id);
    let result;
    result = await this.updateOffer(id , {state:constants.technical.STATE_CANCELLED} ) ;
    return result ;
};


async deleteOffer (id)  {
    logger.info("Call deleteOffer "+id);
    let result;
    result = await offerModel.deleteOne({'_id': id}).exec();
    return result.deletedCount > 0;
};

}

module.exports = new Offer();
