//SERVICE

const subscriptionModel = require("../models/subscriptions");

// LOG 
const loggerGenerator = require("../../../common/core/logger/logger")
const constants = require("../../../common/core/constants/constants")
const  layerName = constants.technical.LAYER_SERVICE
const  servicerName = "subscriptions"
const logger = loggerGenerator.getLoger(layerName,servicerName)


class Subscription  {


    constructor(){
   
      }

async createSubscription (subscription)  {

    logger.info("Call createSubscription");

    const newsubscription = new subscriptionModel(subscription);
    let result;
    result = await newsubscription.save();
    return result;
};

async  getAllSubscriptions(perPage, page, filterParams, pagination = true)  {

    logger.info("Call getAllSubscriptions ");

    let result;
    // delete the not used filterParams
    delete filterParams.limit;
    delete filterParams.page;
    delete filterParams.lang;

    if (filterParams.tags) {
        filterParams.tags = {"$in": filterParams.tags}
    }
    for (let [key, value] of Object.entries(filterParams)) {
        if (typeof value == "string") {
            if ((new Date(value)).toString() === "Invalid Date") {
                filterParams[key] = new RegExp(value + ".*", "i")
            }
        }
    }
    let length = await subscriptionModel.find(filterParams).count();
    //we  handle pagination
    if (pagination === true) {
        result = await subscriptionModel.find(filterParams).limit(perPage).skip(perPage * page).select('-__v').lean().exec();
    } else {
        result = await subscriptionModel.find(filterParams).select('-__v').lean().exec();
    }
    return {"Subscriptions": result, "length": length};
};

async updateSubscription (id, subscription, state = 1) {

    logger.info("Call updateSubscription "+id);

    let result;
    let currentSubscription;

    currentSubscription = await subscriptionModel.findById(id).exec();
    
    if (currentSubscription) {
        currentSubscription.set(subscription);
        // console.log(currentsubscription);
        result = await currentSubscription.save();
        result.toJSON();
        }
    return result;
};


async getSubscriptionById(id) {

    logger.info("Call getSubscriptionById  with id "+id);

    let subscription;

    subscription = await subscriptionModel.findById(id).select('-__v').lean().exec();
    
    logger.info('get subscription service by  id  : '+id);
    return subscription;
};

   async getSubscription(field, value) {

    logger.info("Call getSubscription");

    let subscription;
    // subscription = field === 'email' ? await subscriptionModel.findOne({'email.value': value}).select('-__v').lean().exec() : await subscriptionModel.findById(value).select('-id -password -__v').lean().exec();
    if (field === 'email') {
        subscription = await subscriptionModel.findOne({"email.value": value}).select('-__v').lean().exec();
    } else if (field === 'phoneNumber') {
        subscription = await subscriptionModel.findOne({"phoneNumber.value": value}).select('-__v').lean().exec();
    } else if (field === "random_code_for_processes") {
        subscription = await subscriptionModel.findOne({"random_code_for_processes.requestId": value}).select('-__v').lean().exec();
    } else{
        subscription = await subscriptionModel.findById(value).select('-__v').lean().exec();
    }
    console.log('get subscription service by  field : ' + field + ' and value :' + value);
    return subscription;
};

async cancelSubscription (id)  {

    logger.info("Call cancelSubscription"+id);
    let result;
    result = await this.updateSubscription(id , {state:constants.technical.STATE_CANCELLED} ) ;
    return result ;
};


async deleteSubscription (id)  {
    logger.info("Call deleteSubscription "+id);
    let result;
    result = await subscriptionModel.deleteOne({'_id': id}).exec();
    return result.deletedCount > 0;
};

}

module.exports = new Subscription();
