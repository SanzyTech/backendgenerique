//SERVICE

const orderModel = require("../models/orders");

// LOG 
const loggerGenerator = require("../../../common/core/logger/logger")
const constants = require("../../../common/core/constants/constants")
const  layerName = constants.technical.LAYER_SERVICE
const  servicerName = "orders"
const logger = loggerGenerator.getLoger(layerName,servicerName)


class Order  {


    constructor(){
   
      }

async createOrder (order)  {

    logger.info("Call createOrder");

    const neworder = new orderModel(order);
    let result;
    result = await neworder.save();
    return result;
};

async  getAllOrders(perPage, page, filterParams, pagination = true)  {

    logger.info("Call getAllOrders ");

    let result;
    // delete the not used filterParams
    delete filterParams.limit;
    delete filterParams.page;
    delete filterParams.lang;

    if (filterParams.tags) {
        filterParams.tags = {"$in": filterParams.tags}
    }
    for (let [key, value] of Object.entries(filterParams)) {
        if (typeof value == "string") {
            if ((new Date(value)).toString() === "Invalid Date") {
                filterParams[key] = new RegExp(value + ".*", "i")
            }
        }
    }
    let length = await orderModel.find(filterParams).count();
    //we  handle pagination
    if (pagination === true) {
        result = await orderModel.find(filterParams).limit(perPage).skip(perPage * page).select('-__v').lean().exec();
    } else {
        result = await orderModel.find(filterParams).select('-__v').lean().exec();
    }
    return {"Orders": result, "length": length};
};

async updateOrder (id, order, state = 1) {

    logger.info("Call updateOrder "+id);

    let result;
    let currentOrder;

    currentOrder = await orderModel.findById(id).exec();
    
    if (currentOrder) {
        currentOrder.set(order);
        // console.log(currentorder);
        result = await currentOrder.save();
        result.toJSON();
        }
    return result;
};


async getOrderById(id) {

    logger.info("Call getOrderById  with id "+id);

    let order;

    order = await orderModel.findById(id).select('-__v').lean().exec();
    
    logger.info('get order service by  id  : '+id);
    return order;
};

   async getOrder(field, value) {

    logger.info("Call getOrder");

    let order;
    // order = field === 'email' ? await orderModel.findOne({'email.value': value}).select('-__v').lean().exec() : await orderModel.findById(value).select('-id -password -__v').lean().exec();
    if (field === 'email') {
        order = await orderModel.findOne({"email.value": value}).select('-__v').lean().exec();
    } else if (field === 'phoneNumber') {
        order = await orderModel.findOne({"phoneNumber.value": value}).select('-__v').lean().exec();
    } else if (field === "random_code_for_processes") {
        order = await orderModel.findOne({"random_code_for_processes.requestId": value}).select('-__v').lean().exec();
    } else{
        order = await orderModel.findById(value).select('-__v').lean().exec();
    }
    console.log('get order service by  field : ' + field + ' and value :' + value);
    return order;
};

async cancelOrder (id)  {

    logger.info("Call cancelOrder"+id);
    let result;
    result = await this.updateOrder(id , {state:constants.technical.STATE_CANCELLED} ) ;
    return result ;
};


async deleteOrder (id)  {
    logger.info("Call deleteOrder "+id);
    let result;
    result = await orderModel.deleteOne({'_id': id}).exec();
    return result.deletedCount > 0;
};

}

module.exports = new Order();
