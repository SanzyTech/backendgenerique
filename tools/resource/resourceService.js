//SERVICE

const resourceModel = require("../models/resources");

// LOG 
const loggerGenerator = require("../../../common/core/logger/logger")
const constants = require("../../../common/core/constants/constants")
const  layerName = constants.technical.LAYER_SERVICE
const  servicerName = "resources"
const logger = loggerGenerator.getLoger(layerName,servicerName)


class Resource  {


    constructor(){
   
      }

async createResource (resource)  {

    logger.info("Call createResource");

    const newresource = new resourceModel(resource);
    let result;
    result = await newresource.save();
    return result;
};

async  getAllResources(perPage, page, filterParams, pagination = true)  {

    logger.info("Call getAllResources ");

    let result;
    // delete the not used filterParams
    delete filterParams.limit;
    delete filterParams.page;
    delete filterParams.lang;

    if (filterParams.tags) {
        filterParams.tags = {"$in": filterParams.tags}
    }
    for (let [key, value] of Object.entries(filterParams)) {
        if (typeof value == "string") {
            if ((new Date(value)).toString() === "Invalid Date") {
                filterParams[key] = new RegExp(value + ".*", "i")
            }
        }
    }
    let length = await resourceModel.find(filterParams).count();
    //we  handle pagination
    if (pagination === true) {
        result = await resourceModel.find(filterParams).limit(perPage).skip(perPage * page).select('-__v').lean().exec();
    } else {
        result = await resourceModel.find(filterParams).select('-__v').lean().exec();
    }
    return {"Resources": result, "length": length};
};

async updateResource (id, resource, state = 1) {

    logger.info("Call updateResource "+id);

    let result;
    let currentResource;

    currentResource = await resourceModel.findById(id).exec();
    
    if (currentResource) {
        currentResource.set(resource);
        // console.log(currentresource);
        result = await currentResource.save();
        result.toJSON();
        }
    return result;
};


async getResourceById(id) {

    logger.info("Call getResourceById  with id "+id);

    let resource;

    resource = await resourceModel.findById(id).select('-__v').lean().exec();
    
    logger.info('get resource service by  id  : '+id);
    return resource;
};

   async getResource(field, value) {

    logger.info("Call getResource");

    let resource;
    // resource = field === 'email' ? await resourceModel.findOne({'email.value': value}).select('-__v').lean().exec() : await resourceModel.findById(value).select('-id -password -__v').lean().exec();
    if (field === 'email') {
        resource = await resourceModel.findOne({"email.value": value}).select('-__v').lean().exec();
    } else if (field === 'phoneNumber') {
        resource = await resourceModel.findOne({"phoneNumber.value": value}).select('-__v').lean().exec();
    } else if (field === "random_code_for_processes") {
        resource = await resourceModel.findOne({"random_code_for_processes.requestId": value}).select('-__v').lean().exec();
    } else{
        resource = await resourceModel.findById(value).select('-__v').lean().exec();
    }
    console.log('get resource service by  field : ' + field + ' and value :' + value);
    return resource;
};

async cancelResource (id)  {

    logger.info("Call cancelResource"+id);
    let result;
    result = await this.updateResource(id , {state:constants.technical.STATE_CANCELLED} ) ;
    return result ;
};


async deleteResource (id)  {
    logger.info("Call deleteResource "+id);
    let result;
    result = await resourceModel.deleteOne({'_id': id}).exec();
    return result.deletedCount > 0;
};

}

module.exports = new Resource();
