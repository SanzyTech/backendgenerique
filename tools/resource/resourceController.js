//LIBRARIES

const fs = require('fs');
const path = require('path');
const {validationResult} = require('express-validator');

const errorProcessing = require('../../../../common/core/error/error');
const bcrypt = require('bcrypt');
const processing = require('../../../../common/core/processing/processing');

// SERVICES 


const resourceService = require('../../services/resources');
const roleService = require('../../../../common/api/services/accounts/role');
const email_sender = require('../../../../common/api/services/channels/email');
const smsService = require('../../../../common/api/services/channels/sms');
const ADMIN_RIGHTS = require('../../../../common/core/types/enum').adminRights;
const smsFunctions = require('../../../../common/api/rest/controllers/sms');
const notificationService = require('../../../../common/api/services/channels/notification');

//CONFIGS 

const config = require('../../../../../config/environnement/config');
const no_mail = config.no_mail;
const EMPLOYER = config.permissionLevels.EMPLOYER_USER;
const EMPLOYEE = config.permissionLevels.EMPLOYEE_USER;
const saltRounds = config.sr;

// LOGGERS 

const loggerGenerator = require("../../../../common/core/logger/logger")
const constants = require("../../../../common/core/constants/constants")
const layerName = constants.technical.LAYER_CONTROLLER
const controllerName = "resources"
const logger = loggerGenerator.getLoger(layerName, controllerName)


 class Resource {

    constructor(){
    }

    async getAllResources(req, res) {

        logger.info("Call getAllResources");

        let lang;

        try {
            const limit = req.query.limit && req.query.limit <= 100 ? parseInt(req.query.limit) : 10;
            let preview = req.query.preview ? req.query.preview : false;
            let level = req.query.level  ? parseInt(req.query.level) : 1;
            let page = 0;
            if (req.query) {
                if (req.query.page) {
                    req.query.page = parseInt(req.query.page);
                    page = Number.isInteger(req.query.page) ? req.query.page : 0;
                }
            }
            let Resources;
            let tmp = [];
            let result;

            result = await resourceService.getAllResources(limit, page, req.query, level, preview);
            
            return res.status(200).json({
                'data': result
            });
        } catch (e) {
            logger.info(e.message);
            const err = new errorProcessing.ServerError(e, lang);
            return res.status(500).json({
                'message': errorProcessing.process(err)
            });
        }
    }

async createResource(req, res)  {

    logger.info("Call createResource");

    let message;

    let lang = req.query.lang ? req.query.lang : 'en';

    try {
        //delete field with default value
        
        const resource = await resourceService.createResource(req.body);
        message = lang === 'fr' ? 'Nouveau resource créé ' : 'new resource was created';
        logger.info('new resource was created : ' + resource.id);
        return res.status(200).json({
            'message': message,
            'data': resource,
        });
    } catch (e) {

        logger.info(e.message);
        const err = new errorProcessing.ServerError(e, lang);
        const message = await err.process(false) ;
        return res.status(500).json({
            'message': err.process(message)
        });
    }
}

 async getResourceById(req, res)  {

    logger.info("Call getResourceById") ;

    let lang = req.query.lang ? req.query.lang : 'en';
    let message;

    try {
        let resource = await resourceService.getResourceById(req.params.idResource);
        if (!resource) {
            message = errorProcessing.generateResponse(controllerName, "get", 404, req.query.lang);
            logger.info('resource not found');
            return res.status(404).json({
                'message': message
            });
        }
   
        logger.info('the resource found by id : ' + req.params.idResource);
        return res.status(200).json({
            'data': {"resource": resource}
        });
    } catch (e) {
        logger.info(e.message);
        const err = new errorProcessing.ServerError(e, lang);
        return res.status(500).json({
            'message': errorProcessing.process(err)
        });
    }
};

async updateResource(req, res)  {

    logger.info("Call updateResource");

    let lang = req.query.lang ? req.query.lang : 'en';
  
    delete req.body.state;

    try {

        const resource = await resourceService.updateResource(req.params.idResource, req.body);
        let message;
        if (!resource) {
            message = errorProcessing.generateResponse(controllerName,"update", 404, req.query.lang);
            return res.status(404).json({
                'message': message
            });
        }
        message = errorProcessing.generateResponse(controllerName,"update", 404, req.query.lang);
        logger.info('the Resource was updated by id : ' + req.params.idResource);
        return res.status(200).json({
            'message':message,
            'data': resource,
        });
    } catch (e) {

        logger.info(e.message);
        const err = new errorProcessing.ServerError(e, lang);
        return res.status(500).json({
            'message': errorProcessing.process(err)
        });
    }
};

async cancelResource (req, res){

    logger.info("Call cancelResource");

    let message;
    let lang = req.query.lang ? req.query.lang : 'en';

    try {
        const resourceBeforeDelete = await resourceService.getResource(req.params.idResource);
        // if the resource is already deleted return an error
        message = lang === "fr" ? "ce resource  a déjà été annulé" : "this resource has already been cancelled";
        // check the current resource's state
        if (resourceBeforeDelete.state === "cancelled") {
            return res.status(409).json({
                'message': message
            });
        }

        const resource = await resourceService.cancelResource(req.params.idResource);
        if (resource) {
            //TODO   Notifiy

            // We update stats
            message = req.query.lang === "fr" ? "Suppression du resource " : "the resource was deleted";
            return res.status(200).json({
                'message': message
            });
        } else {
            message = errorProcessing.generateResponse(controllerName,"update", 404, req.query.lang); 
            return res.status(404).json({
                'message': message
            });
        }
    } catch (e) {
        logger.info(e.message);
        const err = new errorProcessing.ServerError(e, lang);
        return res.status(500).json({
            'message': errorProcessing.process(err)
        });
    }
};

async  deleteResource(req, res) {

    logger.info("Call deleteResource");

    let message;
    let lang = req.query.lang ? req.query.lang : 'en';

    try {
        // TO DO ADD A VALIDATOR THAT CHECK IF EXISTS

        const isResourceDeleted = await resourceService.deleteResource(req.params.idResource);
        // if the resource is already deleted return an error
        message = req.query.lang === "fr" ? "le resource ayant pour id : " + req.params.idResource + " a déjà été supprimé " : "the resource identified by id : " + req.params.idResource + " has already been deleted";
        // check the current resource's state
        if (isResourceDeleted === false) {
            return res.status(409).json({
                'message': message
            });
        } else {
            message = req.query.lang === "fr" ? "le resource ayant pour id : " + req.params.idResource + " a  été supprimé " : message = 'the resource identified by id : ' + req.params.idResource + " has  been deleted";
            return res.status(200).json({
                'message': message
            });
        }
    } catch (e) {
        logger.info(e.message);
        const err = new errorProcessing.ServerError(e, lang);
        return res.status(500).json({
            'message': errorProcessing.process(err)
        });
    }
}




}


module.exports = new Resource()