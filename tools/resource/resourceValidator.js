//SERVICE

const resourceModel = require("../models/resource");

// LOG 
const loggerGenerator = require("../../../common/core/logger/logger")
const constants = require("../../../common/core/constants/constants")
const  layerName = constants.technical.LAYER_SERVICE
const  servicerName = "Resource"
const logger = loggerGenerator.getLoger(layerName,servicerName)


class Resource  {


    constructor(){
   
      }

async createResource (resource)  {

    logger.info("Call createResource");

    const newresource = new resourceModel(resource);
    let result;
    result = await newresource.save();
    return result;
};

async  getAllResources(perPage, page, filterParams, pagination = true)  {

    logger.info("Call getAllResources ");

    let result;
    // delete the not used filterParams
    delete filterParams.limit;
    delete filterParams.page;
    delete filterParams.lang;

    if (filterParams.tags) {
        filterParams.tags = {"$in": filterParams.tags}
    }
    for (let [key, value] of Object.entries(filterParams)) {
        if (typeof value == "string") {
            if ((new Date(value)).toString() === "Invalid Date") {
                filterParams[key] = new RegExp(value + ".*", "i")
            }
        }
    }
    let length = await resourceModel.find(filterParams).count();
    //we  handle pagination
    if (pagination === true) {
        result = await resourceModel.find(filterParams).limit(perPage).skip(perPage * page).select('-__v').lean().exec();
    } else {
        result = await resourceModel.find(filterParams).select('-__v').lean().exec();
    }
    return {"Resources": result, "length": length};
};

async updateResource (id, resource, state = 1) {

    logger.info("Call updateResource "+id);

    let result;
        let currentresource;
        if (state === 0) {
            currentresource = await resourceModel.findOne({'email.value': id}).exec();
            if (currentresource && !currentresource.valid) {
                currentresource = null;
            }
        } else {
            currentresource = await resourceModel.findById(id).exec();
        }
    if (currentresource) {
        currentresource.set(resource);
        // console.log(currentresource);
        result = await currentresource.save();
        result.toJSON();
        delete result._id;
        delete result.__v;
        delete result.password;
        }
    return result;
};

   async getResource(field, value) {

    logger.info("Call getResource");

    let resource;
    // resource = field === 'email' ? await resourceModel.findOne({'email.value': value}).select('-__v').lean().exec() : await resourceModel.findById(value).select('-id -password -__v').lean().exec();
    if (field === 'email') {
        resource = await resourceModel.findOne({"email.value": value}).select('-__v').lean().exec();
    } else if (field === 'phoneNumber') {
        resource = await resourceModel.findOne({"phoneNumber.value": value}).select('-__v').lean().exec();
    } else if (field === "random_code_for_processes") {
        resource = await resourceModel.findOne({"random_code_for_processes.requestId": value}).select('-__v').lean().exec();
    } else{
        resource = await resourceModel.findById(value).select('-__v').lean().exec();
    }
    console.log('get resource service by  field : ' + field + ' and value :' + value);
    return resource;
};


async cancelResource (id)  {
    logger.info("Call cancelResourcee "+id);
    let result;
    const resultRole = await RoleModel.deleteOne({'userId': id}).exec();
    if (resultRole) {
        result = await resourceModel.deleteOne({'_id': id}).exec();
        return result.deletedCount > 0;
    } else {
        return false;
    }
};


async deleteResource (id)  {
    logger.info("Call deleteResourcee "+id);
    let result;
    const resultRole = await RoleModel.deleteOne({'userId': id}).exec();
    if (resultRole) {
        result = await resourceModel.deleteOne({'_id': id}).exec();
        return result.deletedCount > 0;
    } else {
        return false;
    }
};

}

module.exports = new Resource();
