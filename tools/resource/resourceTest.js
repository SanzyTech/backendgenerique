let chai = require('chai');
let server = require('../../server');
const supertest = require('supertest');
const should = chai.should();
const api = supertest.agent(server);
const resource = 'resource';
const baseUrl = '/api/v1/';
const url = baseUrl + resource;
global.resourceId = "";
global.resourceId2 = "";


//test insertion for an resource 
describe('POST '+url, () => {
    it('should respond with a success message along with a single resource that was added', (done) => {
        api.post(url + '?lang=en')
            .send(
                {
                    "field1": "value1",
                    "field2": "value2",
                    "field3": "value3",
                    "field4": "value4",
                    "field5": "value5"
                }).set('authorization', 'Bearer ' + global.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                // (indicating that something was "created")
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                //check the message content
                res.body.message.should.eql('new resource was created');

                global.resourceId = res.body.data.id;
                done();
            });
    });
});




// get the resource list shouldn't be empty cause we just add one resource previously
describe('GET '+url, () => {
    it('should respond with a success message along with a get on all the  resources received that were added', (done) => {
        api.get(url + '?lang=en')
            .set('authorization', 'Bearer ' + global.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                //the numbers of resources objects should be not null
                done();
            });
    });
});


//get a single resource

describe('GET '+url+'/:id', () => {
    it('should respond with a success message along with a single get on a resource that was added', (done) => {
        api.get(url + '/' + global.resourceId + '?lang=en')
            .set('authorization', 'Bearer ' + global.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                // (indicating that something was "created")
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');

                done();
            });
    });
});

//get a single resource with false id

describe('GET '+url+'/:id 2', () => {
    const fakeId = '5ds';
    it('should respond with a error message along with a single resource that wasn t added', (done) => {
        api.get(url + '/' + fakeId + '?lang=en')
            .set('authorization', 'Bearer ' + global.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 404 status code
                res.status.should.equal(500);
                // the response should be JSON
                res.type.should.equal('application/json');
                //check the respond code
                done();
            });
    });
});


//get all the evulations from  a specific jober
describe('GET /api/v1/particular/:userId/resources?lang=en', () => {
    it('should respond with a success message along with all resources from a specific jober ', (done) => {
        api.get('/api/v1/particular/' + global.idJober + '/resources?lang=en')
            .set('authorization', 'Bearer ' + global.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                // (indicating that something was "created")
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                //check the message content
                res.body.message.should.eql('All the resources found for the particular id');

                done();
            });
    });
});


//update a resource  , here we first save the id of the resource from the previous get request
describe('UPDATE '+url, () => {
    const grade = 1;
    it('should respond with a success message along with a single resource that was updated', (done) => {
        api.put(url + '/' + global.resourceId + '?lang=en')
            .set('authorization', 'Bearer ' + global.token)
            .send(                {
                    "field1": "value1",
                    "field2": "value2",
                    "field3": "value3",
                    "field4": "value4",
                    "field5": "value5"
                })
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                // (indicating that something was "updated")
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');

                res.body.data.grade.should.equal(grade);
                done();
            });
    });
});

describe('DELETE /api/v1/resource', () => {
    it('should respond with a success message along with a single resource that was deleted', (done) => {
        api.delete(url + '/' + global.resourceId + '?lang=en')
            .set('authorization', 'Bearer ' + global.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                // (indicating that something was "deleted")
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                //check on message
                res.body.message.should.equal('the resource was deleted by id : ' + global.resourceId);
                done();
            });
    });
});



