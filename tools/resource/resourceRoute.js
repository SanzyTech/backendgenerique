const express = require('express');

const resourceController = require('../controllers/resources');

const validationSecurity = require('../../../../common/api/rest/controllers/security/validation');
const permissionSecurity = require('../../../../common/api/rest/controllers/security/permission');
const sanitizeSecurity = require('../../../../common/api/rest/controllers/security/sanitize');

const router = express.Router();
const config = require('../../../../../config/environnement/config');

const ADMIN = config.permissionLevels.ADMIN;
const EMPLOYER = config.permissionLevels.EMPLOYER_Resource;
const EMPLOYEE = config.permissionLevels.EMPLOYEE_Resource;
const CONTROLLER = config.permissionLevels.CONTROLLER_Resource;
const SUP_ADMIN = config.permissionLevels.SUP_ADMIN;
const ENTREPRISE = config.permissionLevels.ENTREPRISE_Resource;

const busboy = require('connect-busboy');
const busboyBodyParser = require('busboy-body-parser');

router.get('/:idResource',
    [
        //validationSecurity.validJWTNeeded,
        //permissionSecurity.permissionLevelRequired([ADMIN, CONTROLLER, SUP_ADMIN]),
        resourceController.getResourceById,
    ]);




    router.post('/',
    [
        //  TO DO implement validation field 
        //sanitizeSecurity.validate('createResource'),
        resourceController.createResource
    ]);




router.put('/:idResource',
    [
       // validationSecurity.validJWTNeeded,
       // permissionSecurity.permissionLevelRequired([ADMIN, CONTROLLER, SUP_ADMIN]),
        resourceController.updateResource
    ]);



router.get('/',
    [
       // validationSecurity.validJWTNeeded,
       // permissionSecurity.permissionLevelRequired([ADMIN, CONTROLLER, SUP_ADMIN]),
        resourceController.getAllResources
    ]);

router.delete('/:idResource',
    [
        //validationSecurity.validJWTNeeded,
        //permissionSecurity.permissionLevelRequired([ADMIN, SUP_ADMIN]),
        resourceController.deleteResource
    ]);



module.exports = router;
