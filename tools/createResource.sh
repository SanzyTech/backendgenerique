#DEFINE VALUE

PROJECT_NAME="template"
ROOT_FOLDER="../src"
MODEL_FOLDER="$ROOT_FOLDER/$PROJECT_NAME/api/models"
SERVICE_FOLDER="$ROOT_FOLDER/$PROJECT_NAME/api/services"
CONTROLLER_FOLDER="$ROOT_FOLDER/$PROJECT_NAME/api/rest/controllers"
ROUTE_FOLDER="$ROOT_FOLDER/$PROJECT_NAME/api/rest/routes"
TEST_FOLDER="$ROOT_FOLDER/../tests"

resource=$1
resourceUpperCase="${resource^}"



# This function takes 4 inputs template  path, destination path and resource name,
createResource() {
if [ "$#" -ne 4 ]; 
    then echo "illegal number of parameters, usage : createRessource templatetype  inputFolder outputFolder resource  "
fi
templatetype=$1
inputFolder=$2
outputFolder=$3
resource=$4
plural="s"
echo  "$inputFolder/$templatetype.js"
echo "$outputFolder/${resource}s.js"
cp   "$inputFolder/$templatetype.js" "$outputFolder/$resource.js_tmp_1"


sed "s/resource/$resource/g" "$outputFolder/$resource.js_tmp_1" > "$outputFolder/$resource.js_tmp_2"

sed "s/Resource/$resourceUpperCase/g" "$outputFolder/$resource.js_tmp_2" > "$outputFolder/$resource.js_tmp_3"

sed "s/Resources/$resourceUpperCases/g" "$outputFolder/$resource.js_tmp_3" > "$outputFolder/$resource.js_tmp_4"

sed "s/resource/$resource/g" "$outputFolder/$resource.js_tmp_4" > "$outputFolder/$resource$plural.js"

rm $outputFolder/$resource.js_tmp*
    
}


#CREATE MODEL

createResource  "resourceModel" "./resource"  $MODEL_FOLDER $resource 

#CREATE SERVICE

createResource  "resourceService" "./resource"  $SERVICE_FOLDER $resource 

#CREATE CONTROLLER

createResource  "resourceController" "./resource"  $CONTROLLER_FOLDER $resource 

#CREATE ROUTE

createResource  "resourceRoute" "./resource"  $ROUTE_FOLDER $resource 

#CREATE TEST
STEP=$(ls $TEST_FOLDER| wc -l)
mkdir "$TEST_FOLDER/${STEP}_${resource}s"
createResource  "resourceTest" "./resource"  "$TEST_FOLDER/${STEP}_${resource}s" $resource 

echo "Resource : $resource created with succes"
