#DEFINE VALUE

PROJECT_NAME="template"
ROOT_FOLDER="../src"
MODEL_FOLDER="$ROOT_FOLDER/$PROJECT_NAME/api/models"
SERVICE_FOLDER="$ROOT_FOLDER/$PROJECT_NAME/api/services"
CONTROLLER_FOLDER="$ROOT_FOLDER/$PROJECT_NAME/api/rest/controllers"
ROUTE_FOLDER="$ROOT_FOLDER/$PROJECT_NAME/api/rest/routes"
TEST_FOLDER="$ROOT_FOLDER/../tests"

resource=$1
resourceUpperCase="${resource^}"



# This function takes 4 inputs template  path, destination path and resource name,
deleteResource() {
if [ "$#" -ne 2 ]; 
    then echo "illegal number of parameters, usage : createRessource  outputFolder resource"
fi

outputFolder=$1
resource=$2
plural="s"

rm $outputFolder/${resource}${plural}.js
   
}

deleteResourceTest() {

if [ "$#" -ne 2 ]; 
    then echo "illegal number of parameters, usage : createRessource  outputFolder resource"
fi
plural="s"
outputFolder="${1}${plural}"
resource="${2}${plural}"

rm -rf "$outputFolder/${resource}.js"
rmdir  "${outputFolder}"   
}


#DELETEMODEL

deleteResource    $MODEL_FOLDER $resource

#DELETESERVICE

deleteResource    $SERVICE_FOLDER $resource

#DELETECONTROLLER

deleteResource    $CONTROLLER_FOLDER $resource

#DELETEROUTE

deleteResource    $ROUTE_FOLDER $resource

#DELETETEST
STEP=$(ls $TEST_FOLDER| wc -l)
STEP="$((STEP-1))"
deleteResourceTest  "$TEST_FOLDER/${STEP}_${resource}" $resource

echo "Resource : $resource delete with succes"
